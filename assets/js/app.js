$(function() {
    // Slider Starts  //
        var ctr = 0;
        var main = $("#MainSlider");
        var item = $("#SliderItems li");
        var max = item.length - 1;

        setInterval(co_slide, 5000);

        function co_slide() {
            if(ctr >= max) {
                ctr = 0;
            } else {
                ctr++;
            }

            var img = item.eq(ctr).prop('src');
            var des = item.eq(ctr).prop('alt');
            $('#location').text(des);

            $("#SliderItems li:eq("+ ctr +")").fadeIn("fast");
            $("#SliderItems li:not(:eq("+ ctr +"))").fadeOut("fast");

            console.log(ctr);

        }
    // Slider Ends //

    // Mobile Starts //
        $('#Mobile-Menu').click(function() {
            $('#Menu-Options').slideToggle("fast");
        });
    // Mobile Ends //


    function base_url() {
        return $('input[name="base_url"]').val();
    }

    $(document).on('change', '.day_photo', function() {
        readURL(this, $(this).attr('data-group'));
    });

    $('#cover_photo').on('change', function() {
        readURL(this, 'cover_photo_preview');
    });

    $('#_addDay').click(function() {
        var source = $("#_DayTemplate").html();
        var template = Handlebars.compile(source);

        var group = +$('input[name="count"]').val() + 1;
        var data = {
            "group" : "group" + group,
            "count" : group
        };

        $("[name='count']").val(group);
        if($('.day-holder').length > 0) {
            $('._deleteDay[data-group="group'+ (group-1) +'"]').hide();
        }

        console.log(group);

        var compiled = template(data);
        $('#day_loader').append(compiled);
        tinyMCE.execCommand('mceAddControl', false, "day_content_"+ group );
        tinyMCE.execCommand('mceAddEditor', false, "day_content_"+ group );

    });

    $(document).on('click', '._deleteDay', function() {
        var group = $(this).attr('data-group');
        var d = group.match(/\d+/);
        $("[name='count']").val($("[name='count']").val()-1);
        $('._deleteDay[data-group="group'+ (parseInt(d[0])-1) +'"]').show();

        tinyMCE.execCommand('mceRemoveControl', false, "day_content_"+ d[0] );
        tinyMCE.execCommand('mceRemoveEditor', false, "day_content_"+ d[0] );
        $('.main-day-holder[data-group="'+ group +'"]').remove();


    });

    $(document).on('click', '._addItinerary', function() {
        var group = $(this).attr('data-group');
        var source = $("#_ItTemplate").html();
        var template = Handlebars.compile(source);
        var data = {
            "group" : group
        };

        var compiled = template(data);
        $('.it_loader[data-group="'+ group +'"]').append(compiled);


    });

    $(document).on('click', '._addExpense', function() {
        var group = $(this).attr('data-group');
        var source = $("#_ExTemplate").html();
        var template = Handlebars.compile(source);
        var data = {
            "group" : group
        };

        console.log(group);

        var compiled = template(data);
        $('.ex_loader[data-group="'+ group +'"]').append(compiled);
    });

    $('#_previewBlog').click(function() {
        var org_act = $('form').attr('action');
        // ReConfig the Form for Preview Setting
        $('form').attr('target', '_blank');
        $('form').attr('action', base_url() + "admin/blog/preview/");
        $('form').submit();
        // ReConfig again and return it to original
        $('form').removeAttr('target');
        $('form').attr('action', org_act);
    });

    $('#blog_title').on('change', function() {
        var v = $(this).val();
        var arr = v.split(' ');
        v = arr.join('-');
        if(v != $('#_temptitle').val()) {
            $.ajax({
                url     : base_url() + "admin/blog/check_slug",
                type    : 'POST',
                data    : {slug : v},
                success : function(data) {
                    data = $.parseJSON(data)
                    $('#blogSlug').text(data.value);
                    $('input[name="blog_slug"]').val(data.value);
                }
            });
        } else {
            $('#blogSlug').text($('input[name="blog_slug"]').val());
            $('#blogSlug').text(v);
            $('input[name="blog_slug"]').val(v);
        }
    }).trigger('change');

    $('#banner_file').on('change', function() {
        var l = this.files.length;

        if(l >= 1)
            $('#BannerSubmit').show();
        else
            $('#BannerSubmit').hide();

        readURL(this, 'banner_preview');
    });

    function readURL(input, id) {
        $("#" + id + " .collage").html("");
        if (input.files) {

            var len = input.files.length;
            if(len > 1) $("#" + id + " .collage").addClass('count-' + len);
            $.each(input.files, function(k, v) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $("#" + id + " .collage").append("<img src='"+ e.target.result +"' />");
                }

                reader.readAsDataURL(input.files[k]);

                if(id == "cover_photo_preview") $('#' + id + " .collage").append("<br/>");
            });

        }
    }
});
