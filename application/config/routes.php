<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller']     = 'home';
$route['admin']                  = 'admin/login';
$route['blog/(:any)']            = 'blog/view/$1';
$route['page/(:any)']            = 'blog/view/$1';
$route['event/(:any)']           = 'blog/view/$1';
$route['events']                  = 'blog/index/event';
$route['blogs']                   = 'blog/index/blog';
$route['404_override']           = '';
$route['translate_uri_dashes']   = FALSE;
