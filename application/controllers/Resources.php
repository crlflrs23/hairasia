<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resources extends CI_Controller {
	public function index($type, $filename, $w = 0, $h = 0) {

		ini_set('memory_limit', '-1');

		if(file_exists('assets/uploads/new_'. $w . 'x' . $h .'_' . $filename)) {
			$data['new_image'] = 'assets/uploads/new_' . $filename;
		} else {
			$this->load->library('image_lib');

			$img = 'assets/uploads/' . $filename;
	        $config['image_library'] = 'gd2';
	        $config['source_image'] = $img;
	        $config['new_image'] = 'assets/uploads/new_'. $w . 'x' . $h .'_' . $filename;
	        $config['quality'] = "80%";
	        $config['maintain_ratio'] = TRUE;


			if($w != 0) {
				$config['width'] = $w;
			}

			if($h != 0) {
				$config['height'] = $h;
			}

	        $this->image_lib->initialize($config);

	        $this->image_lib->resize();

	        $src = $config['new_image'];
	        $data['new_image'] = $src;
	        $data['img_src'] = base_url() . $data['new_image'];
		}

		$file = basename($data['new_image']);
		$file_extension = strtolower(substr(strrchr($file,"."),1));

		switch( $file_extension ) {
		    case "gif": $ctype="image/gif"; break;
		    case "png": $ctype="image/png"; break;
		    case "jpeg":
		    case "jpg": $ctype="image/jpeg"; break;
		    default:
		}

		header('Content-type: ' . $ctype);
		$img = $this->_LoadJpeg($data['new_image']);

		imagejpeg($img);
	}

	public function _LoadJpeg($imgname)
	{
	    /* Attempt to open */
	    $im = @imagecreatefromjpeg($imgname);

	    /* See if it failed */
	    if(!$im)
	    {
	        /* Create a black image */
	        $im  = imagecreatetruecolor(150, 30);
	        $bgc = imagecolorallocate($im, 255, 255, 255);
	        $tc  = imagecolorallocate($im, 0, 0, 0);

	        imagefilledrectangle($im, 0, 0, 150, 30, $bgc);

	        /* Output an error message */
	        imagestring($im, 1, 5, 5, 'Error loading ' . $imgname, $tc);
	    }

	    return $im;
	}
}
