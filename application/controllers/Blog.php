<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {
	public function index($type) {
		$this->load->model('Blog_Model');
		$this->load->helper('Blog');
		$data['type'] = $type;
		$type = $type == "event" ? 3 : 1;
		$data['title']		= "whereyouatkath";
		$data['blogs']		= $this->Blog_Model->get_where('blog', array("blog_status" => 1, "blog_type" => $type));
		$this->load->view('blogs_view', $data);
	}

	public function view($slug) {
		$this->load->model('Blog_Model');
		$q = $this->Blog_Model->get_where('blog', array('blog_slug' => $slug));
		if($q->num_rows() >= 1) {
			$blog_data 	= $q->row_array();
			$data['blog']		= $blog_data;
			$this->load->view('view_blog', $data);
		} else {
			show_404();
		}
	}
}
