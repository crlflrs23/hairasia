<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
	}

	public function index() {
		if($this->session->userdata('author') == null)
			redirect(base_url() . "admin/login");

		$this->load->model("Blog_Model");
		$this->load->helper("Blog");		// Custom Helper Located at Application/helpers

		$data['title']		= "whereyouatkath";
		$data['blogs']		= $this->Blog_Model->get_where('blog', array('blog_type' => 2))->result();
        $data['type']		= "page";
		$this->load->view('admin/blog_view', $data);
	}

	public function compose() {
		if($this->session->userdata('author') == null)
			redirect(base_url() . "admin/login");

		$this->load->model("Blog_Model");
		$this->form_validation->set_rules('blog_title', 'Blog Title', 'required');
		$this->form_validation->set_rules('blog_slug', 'Blog Slug', 'required');

		if(empty($_FILES['cover_photo']['name'])) {
			$this->form_validation->set_rules('cover_photo', 'Cover Photo', 'required');
		}

		$config['upload_path']      = 'assets/uploads/';
		$config['allowed_types']    = 'jpg|png';
		$config['max_size']    		= 0;
		$config['encrypt_name']     = true;

		$this->load->library('upload', $config);

		if($this->form_validation->run()
		&& $this->upload->do_upload('cover_photo')) {

			$blog_data = array(
				"blog_title" 		=> $this->input->post('blog_title'),
				"blog_tags" 		=> $this->input->post('blog_tags'),
				"blog_slug" 		=> $this->input->post('blog_slug'),
				"context_title" 	=> $this->input->post('context_sub_title'),
				"context_content" 	=> $this->input->post('context_content'),
				"author_id" 		=> $this->session->userdata('author')['author_id'],
				"cover_photo"		=> $this->upload->data('file_name'),
				"blog_status"		=> $this->input->post('blog_status'),
                "blog_type"         => 2
			);

			$this->Blog_Model->insert('blog', $blog_data);

			$data['error'] = array(
				"status" 	=> "success",
				"msg"		=> "Successfully composed a page."
			);

		} else {
			$data['error'] = array(
				"status" 	=> "danger",
				"msg"		=> validation_errors()
			);
		}

		$data['title']		= "whereyouatkath";
		$data['type']		= "page";
		$this->load->view('admin/compose_view', $data);
	}

	public function edit($blog_id, $slug) {
		if($this->session->userdata('author') == null)
			redirect(base_url() . "admin/login");

		$this->load->model("Blog_Model");

		$data_condition = array(
			'blog_slug' => $slug,
			'blog_id' 	=> $blog_id
		);
		$q = $this->Blog_Model->get_where('blog', $data_condition);

		$this->form_validation->set_rules('blog_title', 'Blog Title', 'required');
		$this->form_validation->set_rules('blog_slug', 'Blog Slug', 'required');


		$config['upload_path']      = 'assets/uploads/';
		$config['allowed_types']    = 'jpg|png|JPG|PNG';
		$config['max_size']    		= 0;
		$config['encrypt_name']     = true;
		$this->load->library('upload', $config);

		if($this->form_validation->run()) {
			$data_condition = array(
				'blog_slug' => $slug,
				'blog_id' 	=> $blog_id
			);
			$q = $this->Blog_Model->get_where('blog', $data_condition);
			$blog_data 	= $q->row_array();

			if(!empty($_FILES['cover_photo']['name'])) {
				$this->upload->do_upload('cover_photo');
			}

			$blog_data = array(
				"blog_title" 		=> $this->input->post('blog_title'),
				"blog_tags" 		=> $this->input->post('blog_tags'),
				"blog_slug" 		=> $this->input->post('blog_slug'),
				"context_title" 	=> $this->input->post('context_sub_title'),
				"context_content" 	=> $this->input->post('context_content'),
				"author_id" 		=> $this->session->userdata('author')['author_id'],
				"cover_photo"		=> !empty($_FILES['cover_photo']['name']) ? $this->upload->data('file_name') : $blog_data['cover_photo'],
				"blog_status"		=> $this->input->post('blog_status')
			);

			$this->Blog_Model->update('blog', 'blog_id', $blog_id, $blog_data);

			if($this->input->post('count') !== null
			&& $this->input->post('count') != 0) {
				for($i = 1; $i <= $this->input->post('count'); $i++) {


					if(!empty($_FILES['day_photos_' . $i ]['name'][0])) {
						$photo_array = array();
						foreach($_FILES['day_photos_' . $i]['error'] as $k => $v){
							$_FILES['day_photos']['name'] 		= $_FILES['day_photos_' . $i]['name'][$k];
							$_FILES['day_photos']['type'] 		= $_FILES['day_photos_' . $i]['type'][$k];
							$_FILES['day_photos']['tmp_name']	= $_FILES['day_photos_' . $i]['tmp_name'][$k];
							$_FILES['day_photos']['error'] 		= $_FILES['day_photos_' . $i]['error'][$k];
							$_FILES['day_photos']['size'] 		= $_FILES['day_photos_' . $i]['size'][$k];

							$this->upload->initialize($config);
							$this->upload->do_upload('day_photos');
							array_push($photo_array, $this->upload->data('file_name'));
						}
					}

					$day_data = array(
						"blog_id" 		=> $blog_id,
						"day_title" 	=> $this->input->post('day_title_' . $i),
						"day_content" 	=> $this->input->post('day_content_' . $i)
					);

					if(!empty($_FILES['day_photos_' . $i ]['name'][0])) {
						$day_data["day_photos" ] = implode(',', $photo_array);
					}

					if($this->input->post('day_id_' . $i) != 0) {
						$day_id = $this->input->post('day_id_' . $i);
						$this->Blog_Model->update('days', 'day_id', $day_id, $day_data);
					} else {
						$day_id = $this->Blog_Model->insert('days', $day_data);
					}

					foreach($this->input->post('it_description_' . $i) as $k => $v) {
						$it_data = array(
							"day_id" 				=> $day_id,
							"itinerary_time"	 	=> $this->input->post('it_time_' . $i)[$k],
							"itinerary_description" => $v
						);

						if($this->input->post('it_id_' . $i)[$k] != 0) {
							$it_id = $this->input->post('it_id_' . $i)[$k];
							$this->Blog_Model->update('itinerary','itinerary_id', $it_id, $it_data);
						} else {
							$this->Blog_Model->insert('itinerary', $it_data);
						}
					}

					foreach($this->input->post('ex_description_' . $i) as $k => $v) {
						$ex_data = array(
							"day_id" 			  => $day_id,
							"expense_amount"	  => $this->input->post('ex_amount_' . $i)[$k],
							"expense_description" => $v
						);

						if($this->input->post('ex_id_' . $i)[$k] != 0) {
							$ex_id = $this->input->post('ex_id_' . $i)[$k];
							$this->Blog_Model->update('expenses', 'expense_id', $ex_id, $ex_data);
						} else {
							$this->Blog_Model->insert('expenses', $ex_data);
						}
					}
				}
			}

			$data['error'] = array(
				"status" 	=> "success",
				"msg"		=> "Successfully composed a blog."
			);

		} else {
			$data['error'] = array(
				"status" 	=> "danger",
				"msg"		=> validation_errors()
			);
		}

		if($q->num_rows() >= 1) {
			$blog_data 	= $q->row_array();
			$data['title']		= "whereyouatkath";
			$data['blog']		= $blog_data;
			$data['type']		= "page";
			$this->load->view('admin/edit_blog_view', $data);
		} else {
			show_404();
		}
	}

	public function preview() {
		if($this->session->userdata('author') == null)
			redirect(base_url() . "admin/login");

		$this->load->model("Blog_Model");

		$day_data	= array();
		$it_data	= array();
		$ex_data	= array();
		$blog_data  = array();

		$this->form_validation->set_rules('blog_title', 'Blog Title', 'required');

		$config['upload_path']      = 'assets/uploads/';
		$config['allowed_types']    = 'jpg|png|JPG|PNG';
		$config['max_size']    		= 0;
		$config['encrypt_name']     = true;
		$this->load->library('upload', $config);

		if($this->form_validation->run()) {

			if(!empty($_FILES['cover_photo']['name'])) {
				$this->upload->do_upload('cover_photo');
			}

			$blog_data = array(
				"blog_title" 		=> $this->input->post('blog_title'),
				"blog_tags" 		=> $this->input->post('blog_tags'),
				"context_title" 	=> $this->input->post('context_sub_title'),
				"context_content" 	=> $this->input->post('context_content'),
				"author_id" 		=> $this->session->userdata('author')['author_id'],
				"cover_photo"		=> !empty($_FILES['cover_photo']['name']) ? $this->upload->data('file_name') : $this->input->post('txt_cover_photo')
			);

			#$blog_id = $this->Blog_Model->insert('blog', $blog_data);

			if($this->input->post('count') !== null
			&& $this->input->post('count') != 0) {
				for($i = 1; $i <= $this->input->post('count'); $i++) {
					if(!empty($_FILES['cover_photo']['name'])) {
						$photo_array = array();

						foreach($_FILES['day_photos_' . $i]['error'] as $k => $v){
							$_FILES['day_photos']['name'] 		= $_FILES['day_photos_' . $i]['name'][$k];
							$_FILES['day_photos']['type'] 		= $_FILES['day_photos_' . $i]['type'][$k];
							$_FILES['day_photos']['tmp_name']	= $_FILES['day_photos_' . $i]['tmp_name'][$k];
							$_FILES['day_photos']['error'] 		= $_FILES['day_photos_' . $i]['error'][$k];
							$_FILES['day_photos']['size'] 		= $_FILES['day_photos_' . $i]['size'][$k];

							$this->upload->initialize($config);
							$this->upload->do_upload('day_photos');
							array_push($photo_array, $this->upload->data('file_name'));
						}
					}

					$day_data[] = array(
						//"blog_id" 	=> $blog_id,
						"day_title" 	=> $this->input->post('day_title_' . $i),
						"day_content" 	=> $this->input->post('day_content_' . $i),
						"day_photos" 	=> !empty($_FILES['cover_photo']['name']) ? implode(',', $photo_array) : $this->input->post('txt_day_photo_' . $i)
					);

					#$day_id = $this->Blog_Model->insert('days', $day_data);

					foreach($this->input->post('it_description_' . $i) as $k => $v) {
						$it_data["$i"][] = array(
							//"day_id" 				=> $day_id,
							"itinerary_time"	 	=> $this->input->post('it_time_' . $i)[$k],
							"itinerary_description" => $v
						);

						#$this->Blog_Model->insert('itinerary', $it_data);
					}

					foreach($this->input->post('ex_description_' . $i) as $k => $v) {
						$ex_data["$i"][] = array(
							//"day_id" 			  => $day_id,
							"expense_amount"	  => $this->input->post('ex_amount_' . $i)[$k],
							"expense_description" => $v
						);

						#$this->Blog_Model->insert('expenses', $ex_data);
					}
				}
			}

			$data['error'] = array(
				"status" 	=> "success",
				"msg"		=> "Successfully composed a blog."
			);

		} else {
			$data['error'] = array(
				"status" 	=> "danger",
				"msg"		=> validation_errors()
			);
		}

		$data['title']		= "whereyouatkath";
		$data['blog']		= $blog_data;
		$data['day']		= $day_data;
		$data['it']			= $it_data;
		$data['ex']			= $ex_data;
		$this->load->view('admin/blog_preview_view', $data);
	}

	# Checks if the slug does or does not exists.
	# If it does already exists it automatically adds -n+1
	# where n = number of repeatition or loop
	public function check_slug() {
		$this->form_validation->set_rules('slug', 'Slug', 'required|is_unique[blog.blog_slug]');
		if($this->form_validation->run()){
			echo json_encode(array("status" => 1, "value" => $this->input->post('slug'), "msg" => "Slug is available"));
		} else {
			$curr = $this->input->post('slug');
			$i = 0;
			while(true) {
				$i++;
				$temp = $curr . '-' . $i;
				$q = $this->db->get_where('blog', array('blog_slug' => $temp))->num_rows();
				if($q <= 0) {
					echo json_encode(array("status" => 1, "value" => $temp, "msg" => "Slug is available"));
					break;
				}
			}
		}
		exit();
	}

	public function view($slug) {
		$this->load->model('Blog_Model');
		$q = $this->Blog_Model->get_where('blog', array('blog_slug' => $slug));
		$data['title']		= "whereyouatkath";

		if($q->num_rows() >= 1) {
			$blog_data 	= $q->row_array();
			if($blog_data['blog_type'] == 2) {
				$data['blog']		= $blog_data;
				$this->load->view('admin/view_blog', $data);
			} else {
				show_404();
			}
		} else {
			show_404();
		}
	}

	public function delete($id, $slug) {
		$this->load->model('Blog_Model');
		$arr = array(
			"blog_id" 	=> $id,
			"blog_slug" => $slug
		);

		$this->Blog_Model->delete('blog', $arr);

		redirect(base_url() . 'admin/page/');
	}


	public function menu($action, $id) {
		$this->load->model('Blog_Model');

		$q = $this->Blog_Model->get_where('blog', array('blog_id' => $id));
		if($q->num_rows() >= 1) {
			$blog = $q->row();
			$menu_data = array(
				"menu_href" => 'page/' . $blog->blog_slug,
				"menu_name" => $blog->blog_title
			);

			switch ($action) {
				case 0:
					$this->Blog_Model->insert('menus', $menu_data);
					break;
				case 1:
					$this->Blog_Model->delete('menus', array('menu_href' => $menu_data['menu_href']));
					break;
				default:
					# code...
					break;
			}

			redirect(base_url() . 'admin/page');
		}
	}

}
