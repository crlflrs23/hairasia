<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
	}

    public function index() {
        if($this->session->userdata('author') == null)
            redirect(base_url() . "admin/login");

        $this->load->model("Blog_Model");

        $config['upload_path']      = 'assets/uploads/';
		$config['allowed_types']    = 'jpg|png|JPG|PNG';
		$config['max_size']    		= 0;
		$config['encrypt_name']     = true;
		$this->load->library('upload', $config);

        if(isset($_FILES['banner_files']['error'][0])) {
            foreach($_FILES['banner_files']['error'] as $k => $v){
                $_FILES['banner_file']['name'] 		= $_FILES['banner_files']['name'][$k];
                $_FILES['banner_file']['type'] 		= $_FILES['banner_files']['type'][$k];
                $_FILES['banner_file']['tmp_name']	= $_FILES['banner_files']['tmp_name'][$k];
                $_FILES['banner_file']['error'] 	= $_FILES['banner_files']['error'][$k];
                $_FILES['banner_file']['size'] 		= $_FILES['banner_files']['size'][$k];

                $this->upload->initialize($config);
                $this->upload->do_upload('banner_file');

                $this->Blog_Model->insert('banners', array("banner_img" => $this->upload->data('file_name'), "banner_location" => ""));
            }

            $response = array(
                "status"    => 1,
                "msg"       => "Successfully Uploaded a Banner."
            );

            $data['error'] = $response;
        }

        $data['title']		= "whereyouatkath";
		$data['banners']			= $this->Blog_Model->get('banners')->result();
        $this->load->view('admin/banners_view', $data);
    }

    public function delete($id) {
        if($this->session->userdata('author') == null)
            redirect(base_url() . "admin/login");

        $this->load->model("Blog_Model");

        if($id != "") {
            $this->Blog_Model->delete('banners', array('banner_id' => $id));
            redirect(base_url() . 'admin/banner/');
        }
    }

}
