<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct()
   {
	   parent::__construct();
	   $this->load->library('form_validation');
   }

	public function index() {
		if($this->session->userdata('author') != null)
			redirect(base_url() . "admin/dashboard");

		$data['title']		= "whereyouatkath";
		$this->load->view('admin/login_view', $data);
	}

    public function login() {
		$this->load->library('form_validation');
        // Set Rules
        $this->form_validation->set_rules('email', 'Email', 'required|callback_email_check');
        $this->form_validation->set_rules('password', 'Password', 'required');

		if($this->form_validation->run()) {
			redirect(base_url() . "admin/dashboard");
		} else {
			$data['title']		= "whereyouatkath";
			$data['error']		= validation_errors();
			$this->load->view('admin/login_view', $data);
		}
    }

	public function logout() {
		$this->session->sess_destroy();
		redirect(base_url() . "admin/login");
	}

	public function email_check($email) {
		$this->load->model('Author_Model');

		$password = $this->input->post('password');
		$password = md5($password);
		$check = $this->Author_Model->check_credentials($email, $password);
		if($check->num_rows() >= 1) {
			$data = $check->row();
			$session_data = array(
				"author_email" 	=> $data->author_email,
				"author_id" => $data->author_id
			);
			$this->session->set_userdata('author', $session_data);
			return true;
		} else {
			$this->form_validation->set_message('email_check', 'Wrong Email and Password combination.');
			return false;
		}
	}
}
