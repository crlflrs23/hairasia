<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function index() {

		$this->load->model('Blog_Model');
		$this->load->helper('Blog');

		$data['blogs']				= $this->Blog_Model->get_blogs_home(1, 2)->result();
		$data['events']				= $this->Blog_Model->get_blogs_home(3, 2)->result();
		$data['sponsor']			= $this->Blog_Model->get_where('settings', array("setting_code" => "sponsor"))->result();
		$data['magazine']['img']	= $this->Blog_Model->get_where('settings', array("setting_code" => "magazine_img"))->row();
		$data['magazine']['desc']	= $this->Blog_Model->get_where('settings', array("setting_code" => "magazine_description"))->row();
		$data['event']				= $this->Blog_Model->get_blogs_home(3, 1)->row();
		$data['banners']			= $this->Blog_Model->get('banners')->result_array();

		$this->load->view('home_view', $data);
	}
}
