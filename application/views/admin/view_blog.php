<?php
    $this->load->view('admin/includes/header_view');
?>



<?php
     $day 	= $this->Blog_Model->get_where('days', array('blog_id' => $blog['blog_id']))->result_array();
 ?>

<section id="blog">
   <div class="container">
       <div class="row">

           <div class="col-md-12 blog-holder">
               <center>
                   <h1><?=$blog['blog_title']?></h1>
                   <small><?=$blog['date_created'] != null ? date_format(date_create($blog['date_created']), 'd F Y') : date_format(date_create(time()), 'd F Y')?></small>
               </center>
               <br />
                <center><img src="<?=base_url()?>assets/uploads/<?=$blog['cover_photo']?>" alt=""></center>
               <div class="<?=count($day) <= 0 ? 'col-md-offset-2' : ''?> col-md-8">
                   <h3><?=$blog['context_title']?></h3><br/>
                   <div class="blog-description">
                       <?=nl2br($blog['context_content'])?>
                   </div>


                   <?php if (count($day) >= 1): ?>
                         <?php foreach ($day as $key => $v): ?>
                             <h3>Day <?=$key+1?> : <?=$v["day_title"]?></h3>
                             <br/><div class="clearfix"></div>
                             <div class="collage count-<?=count(explode(',', $v['day_photos']))?>">

                                <center>
                                    <?php foreach (explode(',', $v['day_photos']) as $k => $p): ?>
                                        <img src="<?=base_url()?>assets/uploads/<?=$p?>" alt="">
                                    <?php endforeach; ?>
                                </center>

                                 <div class="clearfix"></div>
                             </div>
                             <br/>
                             <div class="blog-description">
                                 <?=$v['day_content']?>
                             </div>
                             <br/><br/>
                         <?php endforeach; ?>
                     <?php endif; ?>

                  <div class="blog-description">
                      <div class="tags">
                          <?=$blog['blog_tags']?>
                      </div>
                  </div>

                                  <br/><br/>
               </div>

               <?php if (count($day) >= 1): ?>
                   <div class="fixed col-md-3">
                       <h3>Itinerary / Expenses</h3>

                       <ul class="itinerary-plot">
                           <?php foreach ($day as $k => $v): ?>
                               <li class="itinerary-item">
                                   <h4>Day <?=$k+1?></h4>
                                   <ul>
                                       <?php
                                            $it 	= $this->Blog_Model->get_where('itinerary', array('day_id' => $v['day_id']))->result_array();
                                        ?>
                                       <?php foreach ($it as $it_k => $it_v): ?>

                                           <li><span class="label label-default"> <?=$it_v["itinerary_time"]?> </span> <?=$it_v["itinerary_description"]?></li>
                                       <?php endforeach; ?>

                                       <li><br/>
                                           <span class="label label-success"> EXPENSES </span> <br/><br/>
                                           <table>
                                               <?php
                                                    $ex 	= $this->Blog_Model->get_where('expenses', array('day_id' => $v['day_id']))->result_array();
                                                ?>
                                               <?php foreach ($ex as $ex_k => $ex_v): ?>
                                                   <tr>
                                                       <td><?=$ex_v["expense_description"]?> </td>
                                                       <td> P <?=number_format($ex_v["expense_amount"], 2)?></td>
                                                   </tr>
                                               <?php endforeach; ?>
                                           </table>
                                       </li>
                                   </ul>
                               </li>
                           <?php endforeach; ?>
                       </ul>
                   </div>
               <?php endif; ?>

           </div>


       </div>
   </div>
</section>
