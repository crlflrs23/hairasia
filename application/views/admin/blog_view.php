<?php
    $this->load->view('admin/includes/header_view');
?>

        <style media="screen">
            button {
                margin-top: 3px;
            }
        </style>
        <section id="blog">
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-2 col-md-9">
                        <a href="<?=base_url()?>admin/<?=$type?>/compose">
                            <button class="pull-right btn btn-success">Compose <?=ucwords($type)?></button>
                        </a>
                    </div>
                    <?php foreach ($blogs as $key => $v): ?>
                        <div class="col-md-12 blog-holder">
                            <center>
                                <h1><?=$v->blog_title?></h1>
                                <small><?=date_format(date_create($v->date_created), 'd F Y')?></small>
                            </center>
                            <br />
                            <div class="col-md-offset-2 col-md-6">
                                <img src="<?=base_url()?>assets/uploads/<?=$v->cover_photo?>">
                            </div>

                            <div class="col-md-3">
                                <div class="blog-description">
                                    <br/>
                                    <p><?=trim_text($v->context_content, 200)?></p>

                                    <?php if ($v->blog_status == 1): ?>
                                        <a href="<?=base_url()?>admin/<?=$type?>/view/<?=$v->blog_slug?>/"><button>Published</button></a>
                                        <br/><a href="<?=base_url()?>admin/<?=$type?>/edit/<?=$v->blog_id?>/<?=$v->blog_slug?>/"><button class="draft">Edit Blog</button></a>
                                    <?php else: ?>
                                        <a href="<?=base_url()?>admin/<?=$type?>/view/<?=$v->blog_slug?>/"><button>Preview</button></a>
                                        <br/><a href="<?=base_url()?>admin/<?=$type?>/edit/<?=$v->blog_id?>/<?=$v->blog_slug?>/"><button class="draft">Edit Draft</button></a>
                                    <?php endif; ?>

                                    <br/><a onclick="var t = confirm('Are you sure you want to delete this <?=$type?> ?'); return t;" href="<?=base_url()?>admin/<?=$type?>/delete/<?=$v->blog_id?>/<?=$v->blog_slug?>/"><button class="delete">Delete</button></a>

                                    <?php if ($type == "page" || $type == "event"): ?>
                                        <?php
                                            $check = $this->Blog_Model->get_where('menus', array("menu_href" => "{$type }/{$v->blog_slug }"));
                                         ?>

                                         <?php if ($check->num_rows() >= 1): ?>
                                             <br/><a href="<?=base_url()?>admin/<?=$type?>/menu/1/<?=$v->blog_id?>"><button type="button" name="button">Remove from Menu</button></a>
                                         <?php else: ?>
                                             <br/><a href="<?=base_url()?>admin/<?=$type?>/menu/0/<?=$v->blog_id?>"><button type="button" name="button">Add to Menu</button></a>
                                         <?php endif; ?>
                                    <?php endif; ?>


                                    <div class="tags">
                                        <br/><br/>
                                        <?=$v->blog_tags?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </section>
    </body>


    <script src="<?=base_url()?>bower_components/jquery/dist/jquery.min.js" charset="utf-8"></script>
    <script src="<?=base_url()?>assets/js/app.js" charset="utf-8"></script>
    <script src="<?=base_url()?>assets/js/bootstrap.min.js" charset="utf-8"></script>
</html>
