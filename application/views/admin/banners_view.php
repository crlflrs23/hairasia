<?php
    $this->load->view('admin/includes/header_view');
?>

        <style media="screen">
            button {
                margin-top: 3px;
            }
        </style>
        <section id="blog">
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-2 col-md-9">
                        <div class="well">
                            <form action="<?=base_url()?>admin/banner/" method="post" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="file">Select Photo/s</label>
                                        <input id="banner_file" type="file" name="banner_files[]" multiple style="display: none;"/><Br/>
                                        <button class="btn btn-primary" id="select-file">Browse Photos</button>
                                    </div>

                                    <div id="banner_preview" class="form-group col-md-9">
                                        <div class="collage">

                                        </div>
                                        <div class="clearfix"></div>
                                    </div>

                                    <center><button id="BannerSubmit" class="btn btn-success" style="display: none;">Upload Photo</button></center>
                                    <div class="clearfix"></div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-offset-2 col-md-9">
                        <div class="row  blog-holder">
                            <?php foreach ($banners as $key => $value): ?>
                                <div class="col-md-3 blog-description">
                                    <center>
                                        <img src="<?=base_url()?>assets/uploads/<?=$value->banner_img?>" alt="">
                                        <a href="<?=base_url()?>admin/banner/delete/<?=$value->banner_id?>"><button class="delete">Delete Photo</button></a>
                                    </center>
                                    <br/>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>

    <script src="<?=base_url()?>bower_components/jquery/dist/jquery.min.js" charset="utf-8"></script>
    <script src="<?=base_url()?>assets/js/app.js" charset="utf-8"></script>
    <script src="<?=base_url()?>assets/js/bootstrap.min.js" charset="utf-8"></script>

    <script type="text/javascript">
        $(function(){
            $('#select-file').click(function() {
                $('#banner_file').trigger('click');
                return false;
            });
        });
    </script>

</html>
