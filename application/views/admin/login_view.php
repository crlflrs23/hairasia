<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="theme-color" content="#000">
        <link rel="icon" sizes="192x192" href="<?=base_url()?>assets/img/ICON.png">

        <title>HairAsia</title>

        <link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?=base_url()?>assets/css/style.min.css">

    </head>
    <body>

     <div class="overlay">
         <div class="container">
             <div class="row">
                 <div class="col-md-12">
                     <div class="title-holder">
                         <center>
                             <h1>HairAsia</h1>
                             <small>ADMIN PANEL</small>
                            </center>
                     </div>
                 </div>
             </div>
         </div>
     </div>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-offset-4 col-md-4">
                    <div class="well">
                        <?php if (isset($error)): ?>
                            <div class="alert alert-danger">
                                <?=$error?>
                            </div>
                        <?php endif; ?>
                        <form action="<?=base_url()?>admin/login/login" method="post">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" id="email" class="form-control input" name="email">
                            </div>

                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" id="password" class="form-control input" name="password">
                            </div>

                            <div class="form-group clearfix">
                                <button class="btn btn-success pull-right">Login</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

</body>

    <div class="container">
        <div class="row">
            <div class="col-md-offset-2 col-md-8">
                <center>

                    <br>
                    <p>Copyright 2016 <span>Carlo Flores</span></p>
                </center>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js" charset="utf-8"></script>
    <script src="assets/js/app.js" charset="utf-8"></script>
    <script src="assets/js/bootstrap.min.js" charset="utf-8"></script>
</html>
