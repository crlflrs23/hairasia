<?php
    $t = $this->Setting_Model->get_where('settings', array('setting_code' => 'title'))->row()->setting_value;
    $st = $this->Setting_Model->get_where('settings', array('setting_code' => 'sub_title'))->row()->setting_value;
 ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="theme-color" content="#000">
        <link rel="icon" sizes="192x192" href="<?=base_url()?>assets/img/ICON.png">

        <title><?=$t?></title>

        <link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?=base_url()?>assets/css/style.min.css">

    </head>
    <body>
        <input type="hidden" name="base_url" value="<?=base_url()?>">
        <nav>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <i id="Mobile-Menu" class="glyphicon glyphicon-menu-hamburger"></i>
                        <ul id="Menu-Options">
                            <li><a href="<?=base_url()?>admin/dashboard">Dashboard</a></li>
                            <li><a href="<?=base_url()?>admin/banner">Banners</a></li>
                            <li><a href="<?=base_url()?>admin/blog">Blogs</a></li>
                            <li><a href="<?=base_url()?>admin/page">Pages</a></li>
                            <li><a href="<?=base_url()?>admin/event">Events</a></li>
                            <li><a href="<?=base_url()?>admin/login/logout">Logout</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>

        <div class="overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-holder">
                            <center>
                                <h1><?=$t?></h1>
                                <small><?=$st?></small>
                               </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
