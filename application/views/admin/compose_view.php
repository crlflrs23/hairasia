<?php
    $this->load->view('admin/includes/header_view');
?>

        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-2 col-md-8">
                        <form action="<?=base_url()?>admin/<?=$type?>/compose/" method="post" enctype="multipart/form-data">

                            <?php if(isset($error) && !empty($error['msg'])): ?>
                                <div class="alert alert-<?=$error['status']?>">
                                    <?=$error['msg']?>
                                </div>
                            <?php endif; ?>

                            <input type="hidden" name="count" value="<?=set_value('count') != null ? set_value('count') : '0'?>">
                            <div class="form-group">
                                <label for="blog_title">Blog Title</label>
                                <input type="text" id="blog_title" value="<?=set_value('blog_title')?>" name="blog_title" class="form-control input">
                                <input type="hidden" name="blog_slug" value="" />
                                <br/><small><span><?=base_url()?><?=$type?>/<span id="blogSlug" style="color: green;"></span></span></small>
                            </div>
                            <div class="row" id="cover_photo_preview">
                                <div class="collage">


                                </div>

                                <div class="clearfix"></div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="form-group col-md-9 col-xs-8">
                                    <label for="context_sub_title">Context Sub Title</label>
                                    <input type="text" id="context_sub_title" value="<?=set_value('context_sub_title')?>" name="context_sub_title" class="form-control input">
                                </div>

                                <div class="form-group col-md-3 col-xs-4">
                                    <label for="cover_photo">Cover Photo</label>
                                    <button class="btn btn-primary browse_cover" type="button">Browse</button>
                                    <input type="file" class="hidden" id="cover_photo" name="cover_photo">
                                </div>
                            </div>

                            <?php if ($type == 'event'): ?>
                                <div class="row">
                                    <div class="form-group col-md-8 col-xs-8">
                                        <label for="event_place">Event Place</label>
                                        <input type="text" id="event_place" value="<?=set_value('event_place')?>" name="event_place" class="form-control input">
                                    </div>

                                    <div class="form-group col-md-4 col-xs-4">
                                        <label for="event_date">Event Date</label>
                                        <input type="date" class="form-control input" id="event_date" name="event_date">
                                    </div>
                                </div>
                            <?php endif; ?>

                            <div class="clearfix"></div>
                            <div class="form-group">
                                <label for="context_content">Context Content</label>
                                <textarea name="context_content" id="context_content" cols="30" rows="10" class="form-control textarea"><?=set_value('context_content')?></textarea>
                            </div>

                            <div class="form-group">
                                <label for="blog_tags"><?= $type == 'event' ? 'Exhibitors' : 'Tags'?></label>
                                <input type="text" class="form-control input" name="blog_tags" id="blog_tags">
                            </div>
                            <br/>

                            <?php if ($type == "blog"): ?>
                                <div class="form-group">
                                    <h2 class="pull-left">Day</h2>
                                    <button type="button" id="_addDay" class="btn btn-xs btn-primary pull-right" style="margin-top: 20px;">Add Day</button>
                                </div>
                            <?php endif; ?>

                            <div class="clearfix"></div><br/>
                            <div id="day_loader">
                                <?php if ((int)set_value('count') >= 1): ?>
                                    <?php
                                        $i = 1;
                                        while($i <= set_value('count')) {
                                    ?>
                                            <div class="col-md-12 main-day-holder" data-group='group<?=$i?>'>
                                                <h3 class="pull-left">Day <?=$i?></h3>
                                                <?php if (set_value('count') == $i): ?>
                                                    <button class="btn btn-xs btn-danger pull-right _deleteDay" data-group="group<?=$i?>" style="margin-top: 10px;" type="button">Remove</button>
                                                <?php endif; ?>
                                                <div class="day-holder">
                                                    <div class="form-group">
                                                        <div class="row blog-holder" id="group<?=$i?>">
                                                            <div class="collage">

                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                        <br/>
                                                        <div class="row">
                                                            <div class="col-md-8 col-xs-7">
                                                                <label for="day_title">Day Title</label>
                                                                <input type="text" id="day_title" name="day_title_<?=$i?>" value="<?=set_value('day_title_' . $i)?>" class="form-control input">
                                                            </div>

                                                            <div class="col-md-4 col-xs-5">
                                                                <label for="day_photos">Day Photos</label><br/>
                                                                <button type="button" class="btn btn-success browse" data-group="group<?=$i?>">Browse Photos</button>
                                                                <input type="file" max="5" style="display: none;" class="day_photo" name="day_photos_<?=$i?>[]" data-group="group<?=$i?>" multiple id="day_photos">
                                                            </div>
                                                        </div>

                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="day_content_<?=$i?>">Day Content</label>
                                                        <textarea name="day_content_<?=$i?>" id="day_content_<?=$i?>" cols="30" rows="10" class="form-control textarea"><?=set_value('day_content_' . $i)?></textarea>
                                                    </div>
                                                    <br/>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <h4 class="pull-left">Itinerary for Day <?=$i?></h4>
                                                                <button type="button" data-group="<?=$i?>" class="_addItinerary btn btn-xs btn-primary pull-right">+1</button>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="it_loader" data-group="<?=$i?>">
                                                                <?php foreach (set_value('it_time_'. $i) as $key => $value): ?>
                                                                    <div class="row">
                                                                        <div class="form-group col-md-4 col-xs-4">
                                                                            <label for="it_day1">Time</label>
                                                                            <input id="it_day1" type="time" name="it_time_<?=$i?>[]"  value='<?=$value?>' class="form-control input">
                                                                        </div>
                                                                        <div class="form-group col-md-8 col-xs-8">
                                                                            <label for="it_description1">Description</label>
                                                                            <input id="it_description1" type="text" name="it_description_<?=$i?>[]" value='<?=set_value('it_description_'.$i)[$key]?>' class="form-control input">
                                                                        </div>
                                                                    </div>
                                                                <?php endforeach; ?>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <h4 class="pull-left">Expenses for Day <?=$i?></h4>
                                                                <button type="button" data-group="<?=$i?>" class="_addExpense btn btn-xs btn-primary pull-right">+1</button>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="ex_loader" data-group="<?=$i?>">
                                                                <?php foreach (set_value('ex_description_' . $i) as $key => $value): ?>
                                                                    <div class="row">
                                                                        <div class="form-group col-md-8 col-xs-8">
                                                                            <label for="ex_description1">Description</label>
                                                                            <input id="ex_description1" type="text" value="<?=$value?>" name="ex_description_<?=$i?>[]" class="form-control input">
                                                                        </div>
                                                                        <div class="form-group col-md-4 col-xs-4">
                                                                            <label for="ex_amount1">Amount</label>
                                                                            <input id="ex_amount1" type="number" name="ex_amount_<?=$i?>[]" value="<?=set_value('ex_amount_'. $i)[$key]?>" class="form-control input">
                                                                        </div>
                                                                    </div>
                                                                <?php endforeach; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr />
                                            </div>
                                    <?php
                                            $i++;
                                        }
                                     ?>
                                <?php endif; ?>
                            </div>

                            <center>
                                <button name="blog_status" value="1" id="_publishBlog" class="btn btn-md btn-success">Publish</button>
                                <button name="blog_status" value="0" id="_draftBlog" class="btn btn-md btn-warning">Draft</button>
                                <button type="button" id="_previewBlog" class="btn btn-md btn-info">Preview</button>
                            </center>
                            <br/><br/>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </body>


    <script id="_DayTemplate" type="text/x-handlebars-template">
        <div class="col-md-12 main-day-holder" data-group='{{ group }}'>
            <h3 class="pull-left">Day {{ count }}</h3>
            <button class="btn btn-xs btn-danger pull-right _deleteDay" data-group="{{ group }}" style="margin-top: 10px;" type="button">Remove</button>
            <div class="day-holder">
                <div class="form-group">
                    <div class="row blog-holder" id="{{ group }}">
                        <div class="collage">

                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-md-8 col-xs-7">
                            <label for="day_title">Day Title</label>
                            <input type="text" id="day_title" name="day_title_{{ count }}" class="form-control input">
                        </div>

                        <div class="col-md-4 col-xs-5">
                            <label for="day_photos">Day Photos</label><br/>
                            <button type="button" class="btn btn-success browse" data-group="{{ group }}">Browse Photos</button>
                            <input type="file" max="5" style="display: none;" class="day_photo" name="day_photos_{{ count }}[]" data-group="{{ group }}" multiple id="day_photos">
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <label for="day_content_{{ count }}">Day Content</label>
                    <textarea name="day_content_{{ count }}" id="day_content_{{ count }}" cols="30" rows="10" class="form-control textarea"></textarea>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <h4 class="pull-left">Itinerary for Day {{ count }}</h4>
                            <button type="button" data-group="{{ count }}" class="_addItinerary btn btn-xs btn-primary pull-right">+1</button>
                        </div>
                        <div class="clearfix"></div>
                        <div class="it_loader" data-group="{{ count }}">
                            <div class="row">
                                <div class="form-group col-md-4 col-xs-4">
                                    <label for="it_day1">Time</label>
                                    <input id="it_day1" type="time" name="it_time_{{ count }}[]" class="form-control input">
                                </div>
                                <div class="form-group col-md-8 col-xs-8">
                                    <label for="it_description1">Description</label>
                                    <input id="it_description1" type="text" name="it_description_{{ count }}[]" class="form-control input">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <h4 class="pull-left">Expenses for Day {{ count }}</h4>
                            <button type="button" data-group="{{ count }}" class="_addExpense btn btn-xs btn-primary pull-right">+1</button>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ex_loader" data-group="{{ count }}">
                            <div class="row">
                                <div class="form-group col-md-8 col-xs-8">
                                    <label for="ex_description1">Description</label>
                                    <input id="ex_description1" type="text" name="ex_description_{{ count }}[]" class="form-control input">
                                </div>
                                <div class="form-group col-md-4 col-xs-4">
                                    <label for="ex_amount1">Amount</label>
                                    <input id="ex_amount1" type="number" name="ex_amount_{{ count }}[]" class="form-control input">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr />
        </div>
    </script>

    <script id="_ItTemplate" type="text/x-handlebars-template">
        <div class="row">
            <div class="form-group col-md-4 col-xs-4">
                <label for="it_day1">Time</label>
                <input id="it_day1" type="time" name="it_time_{{ group }}[]" class="form-control input">
            </div>
            <div class="form-group col-md-8 col-xs-8">
                <label for="it_description1">Description</label>
                <input id="it_description1" type="text" name="it_description_{{ group }}[]" class="form-control input">
            </div>
        </div>
    </script>

    <script id="_ExTemplate" type="text/x-handlebars-template">
        <div class="row">
            <div class="form-group col-md-8 col-xs-8">
                <label for="ex_description1">Description</label>
                <input id="ex_description1" type="text" name="ex_description_{{ group }}[]" class="form-control input">
            </div>
            <div class="form-group col-md-4 col-xs-4">
                <label for="ex_amount1">Amount</label>
                <input id="ex_amount1" type="number" name="ex_amount_{{ group }}[]" class="form-control input">
            </div>
        </div>
    </script>


    <script src="<?=base_url()?>bower_components/jquery/dist/jquery.min.js" charset="utf-8"></script>
    <script src="<?=base_url()?>bower_components/handlebars/handlebars.min.js" charset="utf-8"></script>
    <script src="<?=base_url()?>assets/js/bootstrap.min.js" charset="utf-8"></script>
    <script src="<?=base_url()?>assets/js/tinymce/tinymce.min.js" charset="utf-8"></script>
    <script src="<?=base_url()?>assets/js/app.js" charset="utf-8"></script>

    <script>
        tinymce.init({
            selector:'textarea',
            menubar : 'false',
            statusbar: 'false',
            plugins : 'autoresize',
            autoresize_bottom_margin: 10
        });



        $(function() {
            $(document).on('click', '.browse', function() {
                var g = $(this).attr('data-group');
                $(".day_photo[data-group='"+ g +"']").trigger('click');
            });

            $(document).on('click', '.browse_cover', function() {
                $("#cover_photo").trigger('click');
            });
        });

    </script>

</html>
