<?php
    $this->load->view('admin/includes/header_view');
?>

        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="view-info">
                            <div class="title">Direct</div>
                            <div class="value">36,212</div>
                            <div class="footer">New this week <span class="footer-value"> +10,203 </span></div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="view-info">
                            <div class="title">Facebook</div>
                            <div class="value">36,212</div>
                            <div class="footer">New this week <span class="footer-value"> +10,203 </span></div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="view-info">
                            <div class="title">Twitter</div>
                            <div class="value">36,212</div>
                            <div class="footer">New this week <span class="footer-value"> +10,203 </span></div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="view-info">
                            <div class="title">Total</div>
                            <div class="value">129,242</div>
                            <div class="footer">New this week <span class="footer-value"> +10,203 </span></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="blog">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 blog-holder">
                        <center>
                            <h1>Davao-Bukidnon Trip</h1>
                            <small>2nd January, 2017</small>
                        </center>
                        <br />
                        <div class="col-md-offset-2 col-md-6">
                            <img src="assets/img/sample5.jpg">
                        </div>

                        <div class="col-md-3">
                            <div class="blog-description">
                                <br/>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae neque, blanditiis. Nemo molestias voluptatum tenetur, soluta nostrum rerum, similique velit quidem esse recusandae sequi expedita ipsum fugiat modi minus at.</p>
                                <button>Read More</button>

                                <div class="tags">
                                    <br/><br/>
                                    Travel2016, DavaoBukidnonTrip
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 blog-holder">
                        <center>
                            <h1>Samal Island Escapade</h1>
                            <small>1st January, 2017</small>
                        </center>
                        <br />
                        <div class="col-md-offset-2 col-md-6">
                            <img src="assets/img/sample1.jpg">
                        </div>

                        <div class="col-md-3">
                            <div class="blog-description">
                                <br/>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae neque, blanditiis. Nemo molestias voluptatum tenetur, soluta nostrum rerum, similique velit quidem esse recusandae sequi expedita ipsum fugiat modi minus at.</p>
                                <button>Read More</button>

                                <div class="tags">
                                    <br/><br/>
                                    Travel2016, DavaoBukidnonTrip
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js" charset="utf-8"></script>
    <script src="assets/js/app.js" charset="utf-8"></script>
    <script src="assets/js/bootstrap.min.js" charset="utf-8"></script>
</html>
