
    <?php
        $this->load->view('includes/header_view.php');
        $this->load->view('includes/menu_view.php');
    ?>
        <br/><br/><Br/>
        <section id="blog">
            <div class="container">
                <div class="row">
                    <?php foreach ($blogs->result() as $key => $v): ?>
                        <div class="col-md-12 blog-holder">
                            <div class="row">
                                <div class="col-md-offset-2 col-md-8">
                                    <center>
                                        <h1><?=$v->blog_title?></h1>
                                        <small><?=date_format(date_create($v->date_created), 'd F Y')?></small>
                                    </center>
                                </div>
                            </div>
                            <br />
                            <div class="col-md-offset-2 col-md-6">
                                <img src="<?=base_url()?>assets/uploads/<?=$v->cover_photo?>">
                            </div>

                            <div class="col-md-3">
                                <div class="blog-description">
                                    <br/>
                                    <p><?=trim_text($v->context_content, 200)?></p>

                                    <a href="<?=base_url()?><?=$type?>/<?=$v->blog_slug?>/"><button class='m-btn bordered'>Read More</button></a>

                                    <div class="tags">
                                        <br/><br/>
                                        <p><strong>EXHIBITORS </strong></p>
                                        <?=$v->blog_tags?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </section>
    </body>


    <?php $this->load->view('includes/footer_view.php');  ?>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js" charset="utf-8"></script>
    <script src="assets/js/app.js" charset="utf-8"></script>
    <script src="assets/js/bootstrap.min.js" charset="utf-8"></script>
</html>
