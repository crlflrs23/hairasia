<nav>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-3">
                    <span class="logo"><span class="first-part">HAIR</span><span class="sec-part">ASIA</span></span>
                    <i id="Mobile-Menu" class="pull-right glyphicon glyphicon-menu-hamburger" style="font-size: 24px; margin-top: 10px; display: none;"></i>
                </div>

                <?php
                    $type = $this->uri->segment(1);
                    $final = $this->uri->segment(1) . '/' .  $this->uri->segment(2);
                 ?>

                <div id="Menu-Options" class="col-xs-12 col-md-offset-1 col-md-8">
                    <ul class="main-menu pull-right">
                        <li class="menu-item <?=$type == null ? "active" : ''?>"><a href="<?=base_url()?>">HOME</a></li>

                        <?php
                            $this->load->model('Blog_Model');
                            $q = $this->Blog_Model->get('menus')->result();
                        ?>

                        <?php foreach ($q as $key => $value): ?>
                            <li class="<?=$final == $value->menu_href ? "active" : ''?>"><a href="<?=base_url()?><?=$value->menu_href?>/"><?=$value->menu_name?></a></li>
                        <?php endforeach; ?>

                        <li class="menu-item <?=$type == "blogs"  || $type == "blog" ? "active" : ''?>"><a href="<?=base_url()?>blogs">Blogs</a></li>
                        <li class="menu-item <?=$type == "events" || $type == "event" ? "active" : ''?>"><a href="<?=base_url()?>events">Events</a></li>

                        <li class="menu-item <?=$type == "contact-us" ? "active" : ''?>"><a href="<?=base_url()?>">Contact Us</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>
