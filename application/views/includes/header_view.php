<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="theme-color" content="#000">
    <link rel="icon" sizes="192x192" href="<?=base_url()?>assets/img/ICON.png">

    <title>HairAsia</title>


    <link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/master.min.css">
</head>
<body>
<?php $this->load->view('includes/menu_view')?>
