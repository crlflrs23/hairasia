</body>

<footer>
    <center>
        <ul class="footer-menu">
            <li class="menu-item"><a href="<?=base_url()?>">HOME</a></li>
            <?php
                $this->load->model('Blog_Model');
                $q = $this->Blog_Model->get('menus')->result();
            ?>

            <?php foreach ($q as $key => $value): ?>
                <li class="menu-item"><a href="<?=base_url()?><?=$value->menu_href?>/"><?=$value->menu_name?></a></li>
            <?php endforeach; ?>
        </ul>

        <p>All Rights Reserved <span style="color: #bb1515;">HAIRASIA</span> Copyright 2017</p>
        <br/>
        <img src="<?=base_url()?>assets/complexus.png" width="100" alt="">
    </center>
</footer>

<script src="<?=base_url()?>bower_components/jquery/dist/jquery.min.js" charset="utf-8"></script>
<script src="<?=base_url()?>assets/js/app.js" charset="utf-8"></script>
</html>
