<?php
	$ci =& get_instance();
	$ci->load->model('Setting_Model');
	$t = $ci->Setting_Model->get_where('settings', array('setting_code' => 'title'))->row();
	$st = $ci->Setting_Model->get_where('settings', array('setting_code' => 'sub_title'))->row();
 ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="theme-color" content="#000">
        <link rel="icon" sizes="192x192" href="<?=base_url()?>assets/img/ICON.png">

        <title>#whereyouatkath</title>

        <link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?=base_url()?>assets/css/style.min.css">
		<style media="screen">
			h1 {
				font-size: 63px;
			}

			h3 {
				font-size: 24px;
			}
		</style>

    </head>
    <body>
		<div class="overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-holder">
                            <center>
                                <h1><?=$t->setting_value?></h1>
                                <small><?=$st->setting_value?></small>
                               </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>

		<div class="container">
			<div class="row">
				<div class="col-md-offset-4 col-md-4">
					<center>
						<h1>Oops!</h1>
						<h3>We can't seem to find the page you're looking for.</h3><Br/>
						<a href="<?=base_url()?>"><button class="btn btn-primary">Take me to Homepage</button></a>
					</center>
				</div>
			</div>
		</div>
	</body>
	<br/><Br/><Br/>
	<br/><Br/><Br/>
	<br/><Br/>
	<footer>
	    <div class="container">
	        <div class="row">
	            <div class="col-md-offset-2 col-md-8">
	                <center>
	                    <ul class="footer-menu">
	                        <li><a href="<?=base_url()?>">Home</a></li>
	                        <li><a href="#">Blogs</a></li>
	                        <li><a href="#">About Kath</a></li>
	                    </ul>
	                    <br>
	                    <p>Copyright 2016 <span>Kath Hinlog</span></p>
	                </center>
	            </div>
	        </div>
	    </div>
	</footer>


</html>
