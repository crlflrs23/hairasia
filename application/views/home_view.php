<?php
    $this->load->view('includes/header_view');
?>
    <div class="container">
        <div class="row">
            <section id="MainSlider">
                <ul id="SliderItems">
                    <?php foreach ($banners as $key => $value): ?>
                        <li style="background: url('<?=base_url()?>resources/index/img/<?=$value['banner_img']?>/1366/')"></li>
                    <?php endforeach; ?>
                </ul>

                <?php if (count($event) >= 1): ?>
                    <div class="overlay">
                        <div class="container">
                            <div class="row">
                                <div class="content-holder col-md-offset-6 col-md-5">
                                    <div class="overlay-holder">
                                        <center>
                                            <h4><?=$event->blog_title?></h4>
                                            <h1><?=date_format(date_create($event->blog_start_date), 'd F Y')?></h1>
                                            <h4><?=$event->blog_place?></h4>
                                            <p><?=$event->blog_tags?></p>
                                            <a href="<?=base_url()?>event/<?=$event->blog_slug?>"><button class="m-btn bordered">READ MORE</button></a>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </section>
        </div>
    </div>

    <div class="container main-content">
        <div class="row">
            <!-- Main Left Contents -->
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                        <div class="header-title">Events and Exhibits</div>
                    </div>

                    <?php if (count($events) >= 1): ?>
                        <?php foreach ($events as $key => $value): ?>
                            <div class="col-xs-6 col-md-6 header-content">
                                <div class="img-holder">
                                    <img src="<?=base_url()?>resources/index/img/<?=$value->cover_photo?>/600" alt="">
                                </div>
                                <p><?=$value->blog_title?></p>
                                <small><?=date_format(date_create($value->date_created), 'd F Y')?></small><br/><br/>
                                <a href="<?=base_url()?>event/<?=$value->blog_slug?>"><button class="m-btn bordered">Read More</button></a>
                            </div>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <center>
                            <i class="glyphicon glyphicon-exclamation-sign" style="font-size: 48px;"></i>
                            <p>Please write some content on Admin Panel.</p>
                        </center>
                    <?php endif; ?>
                </div>
                <br/><br/>
                <?php if (count($blogs) >= 1): ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="header-title">HairAsia Blogs</div>
                        </div>

                        <?php foreach ($blogs as $key => $value): ?>
                            <div class="col-xs-6 col-md-6 header-content">
                                <div class="img-holder">
                                    <img src="<?=base_url()?>resources/index/img/<?=$value->cover_photo?>/600" alt="">
                                </div>
                                <p><?=$value->blog_title?></p>
                                <small><?=date_format(date_create($value->date_created), 'd F Y')?></small><br/><br/>
                                <a href="<?=base_url()?>event/<?=$value->blog_slug?>"><button class="m-btn bordered">Read More</button></a>
                            </div>
                        <?php endforeach; ?>

                    </div>

                    <br/><br/>
                <?php endif; ?>
            </div>

            <!-- Main Right Contents -->
            <div class="col-md-4">
                <?php if (count($sponsor) >= 1): ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="header-title">Sponsor</div>
                        </div>

                        <div class="col-md-12 header-content">
                            <center>
                                <img src="<?=base_url()?>assets/uploads/sponsor/<?=$sponsor->setting_value?>" alt="">
                            </center>
                        </div>
                    </div>
                    <br/><br/>
                <?php endif; ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="header-title">Our Latest Magazine</div>
                    </div>

                    <div class="col-md-12 header-content">
                        <center>
                            <img src="<?=base_url()?>assets/uploads/<?=$magazine['img']->setting_value?>" alt=""><br/><br/>
                            <p><?=$magazine['desc']->setting_value?></p>
                        </center>
                    </div>

                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('includes/footer_view'); ?>
