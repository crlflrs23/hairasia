<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog_Model extends CI_Model {

    function get($table) {
        return $this->db->get($table);
    }

    function get_where($table, $arr_cond) {
        return $this->db->get_where($table, $arr_cond);
    }

    function insert($table, $data) {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    function update($table, $col, $id, $data) {
        $this->db->where($col, $id);
        $this->db->update($table, $data);
        return $this->db->affected_rows();
    }

    function delete($table, $arr_cond) {
        $this->db->where($arr_cond);
        $this->db->delete($table);
        return $this->db->affected_rows();
    }

    function get_blogs_home($type, $limit) {
        $this->db->where('blog_type', $type);
        $this->db->order_by('date_created');
        $this->db->limit($limit);
        return $this->db->get('blog');
    }
}
