-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 14, 2017 at 10:15 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cmplxs_whereyouatkath`
--

-- --------------------------------------------------------

--
-- Table structure for table `ha_authors`
--

CREATE TABLE `ha_authors` (
  `author_id` int(11) NOT NULL,
  `author_email` varchar(150) NOT NULL,
  `author_password` varchar(150) NOT NULL,
  `author_first_name` varchar(60) NOT NULL,
  `author_last_name` varchar(60) NOT NULL,
  `author_middle_name` varchar(60) NOT NULL,
  `author_second_email` varchar(450) NOT NULL,
  `author_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ha_authors`
--

INSERT INTO `ha_authors` (`author_id`, `author_email`, `author_password`, `author_first_name`, `author_last_name`, `author_middle_name`, `author_second_email`, `author_status`) VALUES
(1, 'whereyouatkath@gmail.com', '5f4dcc3b5aa765d61d8327deb882cf99', 'Kathleen', 'Hinlog', 'Bucao', 'carlo29092@gmail.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ha_banners`
--

CREATE TABLE `ha_banners` (
  `banner_id` int(11) NOT NULL,
  `banner_img` varchar(200) NOT NULL,
  `banner_location` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ha_blog`
--

CREATE TABLE `ha_blog` (
  `blog_id` int(11) NOT NULL,
  `blog_title` varchar(250) NOT NULL,
  `blog_slug` varchar(150) NOT NULL,
  `blog_tags` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `context_title` varchar(250) NOT NULL,
  `context_content` text NOT NULL,
  `cover_photo` varchar(150) NOT NULL,
  `author_id` int(11) NOT NULL,
  `blog_status` int(11) NOT NULL DEFAULT '2',
  `blog_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 = Blog, 2 = Page, 3 = Event'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ha_blog`
--

INSERT INTO `ha_blog` (`blog_id`, `blog_title`, `blog_slug`, `blog_tags`, `date_created`, `date_modified`, `context_title`, `context_content`, `cover_photo`, `author_id`, `blog_status`, `blog_type`) VALUES
(18, 'Stranger to Strangers', 'Stranger-to-Strangers', 'love, random, thoughts', '2017-01-31 18:32:21', '0000-00-00 00:00:00', '', '<p style="text-align: center;">I always wonder if the &ldquo;stranger to strangers&rdquo; really exist because according to what I have researched about love there is such a thing called &ldquo;Love Cycle&rdquo; from a stranger to friends, friends to bestfriends, bestfriends to lovers, lovers to ex lovers and ex lovers to strangers. I don&rsquo;t really understand why it exist maybe because young nowadays are fond on finding true love but every time they enter into a relationship they always fail. Failure in love makes other feel more wanted and some of them can&rsquo;t feel the pain after break up because they are too numb for that. I always ask my mom if I can find love but she always reply that some other time I will truly understand what love is. Now that I&rsquo;ve grown up I ask that question again not my mom but to myself.&nbsp;</p>\r\n<p style="text-align: center;">Can I find love? Can I really find love? Nah. I think no it&rsquo;s not because I am not into a relationship or commitment but it sounds desperate if you find love and I know true love can wait. Yes, we can find love from family, friends and co-workers but there will be always a space in your heart where you want to find your true love, partner in life and a lifetime lover. It&rsquo;s a matter of choice if you are willing to wait or not because for me all the things will happen or happened to me is worth a wait.&nbsp;<br /><br />If you&rsquo;ll ask me if how many times I&rsquo;ve been into a relationship my answer is once it lasted for almost 5 years and now its been 3 years since we broke up. For 3 years, I didn&rsquo;t search for a new love because I was too contented from the love that my friends, family and co-workers gave to me but last March I didn&rsquo;t expect that love will find me. I never asked for it but within one snap love came to me. I never regret my decision to be in a relationship now because what I have said earlier I never asked for it but I consider it as a blessing from above and looking forward that it would be the last relationship I&rsquo;ll be into.&nbsp;<br /><br />I always hear some whims from my friends that there is right love at the wrong time but I don&rsquo;t believe on it because it should be right love at the right time. There is no such thing as wrong time because I believe that true love will find a way when the time will come you can feel its presence it would be as magical like in fairy tales and no one can stop you from your happiness. Although for other the wrong time means about the having a relationship at the very young age or having relationship to whom you love but it&rsquo;s against all odds but why rush things on finding love? why defend love when it&rsquo;s against to all odds? We don&rsquo;t need to rush things because every step we will take in our lives is according to Gods plan. We don&rsquo;t need also to defend our love from others because at first, they will not be against if they didn&rsquo;t see anything wrong between you and your love and why will they stop you from being happy when they are confident that you will not be in pain in the future.&nbsp;<br /><br />To whom should I fall in love? that&rsquo;s a question from one of my friends in school. I didn&rsquo;t answer her question that time because I don&rsquo;t really &nbsp;know what&rsquo;s the best answer for that. Although I have an answer in my mind and that is you don&rsquo;t really know to whom should you fall in love because you can&rsquo;t control your feelings. Yes, you might prevent it from falling but still no cure for that feeling. Lets just accept the fact that it&rsquo;s not always whom we want but its all about whom we are destined for.&nbsp;<br /><br />Going back to the cycle of love, as I said I always wonder if there is such a thing like that. Honestly speaking that cycle of love is just a result of being too numb and too crazy about love. Why do I say so? It&rsquo;s because yes, you started as a stranger but it&rsquo;s not appropriate to end it as a strangers also. You have spent days, month and years with that person, you share all your problems with each other and yet you&rsquo;ll just waste your friendship because you separated as lovers? I know it&rsquo;s not that easy to move on but its easy to accept the reality. If you easily accept reality its a way on how to move on. Accept the fact that you will be friends, accept the fact that you&rsquo;ll not be together again. It&rsquo;s just all about accepting all the result of your choice. Love is not all about a &ldquo;win-win&rdquo; game but Love is all about give and take.<br /><br />One of my closest friends asked me if I am afraid to fall in love. I answer her question with a smile and said that I am not afraid to fall in love. Why? because the feeling of being in love and being loved by other is the best feeling I have ever felt.&nbsp;<br /><br />Random thoughts,<br />- KATH</p>', 'b8bb309b04db231681fea7fc733320d7.jpg', 1, 1, 1),
(19, 'Stranger to Strangers', 'Stranger-to-Strangers-1', 'love, random, thoughts', '2017-01-31 18:36:08', '0000-00-00 00:00:00', '', '<p style="text-align: center;">I always wonder if the &ldquo;stranger to strangers&rdquo; really exist because according to what I have researched about love there is such a thing called &ldquo;Love Cycle&rdquo; from a stranger to friends, friends to bestfriends, bestfriends to lovers, lovers to ex lovers and ex lovers to strangers. I don&rsquo;t really understand why it exist maybe because young nowadays are fond on finding true love but every time they enter into a relationship they always fail. Failure in love makes other feel more wanted and some of them can&rsquo;t feel the pain after break up because they are too numb for that. I always ask my mom if I can find love but she always reply that some other time I will truly understand what love is. Now that I&rsquo;ve grown up I ask that question again not my mom but to myself.&nbsp;</p>\r\n<p style="text-align: center;">Can I find love? Can I really find love? Nah. I think no it&rsquo;s not because I am not into a relationship or commitment but it sounds desperate if you find love and I know true love can wait. Yes, we can find love from family, friends and co-workers but there will be always a space in your heart where you want to find your true love, partner in life and a lifetime lover. It&rsquo;s a matter of choice if you are willing to wait or not because for me all the things will happen or happened to me is worth a wait.&nbsp;<br /><br />If you&rsquo;ll ask me if how many times I&rsquo;ve been into a relationship my answer is once it lasted for almost 5 years and now its been 3 years since we broke up. For 3 years, I didn&rsquo;t search for a new love because I was too contented from the love that my friends, family and co-workers gave to me but last March I didn&rsquo;t expect that love will find me. I never asked for it but within one snap love came to me. I never regret my decision to be in a relationship now because what I have said earlier I never asked for it but I consider it as a blessing from above and looking forward that it would be the last relationship I&rsquo;ll be into.&nbsp;<br /><br />I always hear some whims from my friends that there is right love at the wrong time but I don&rsquo;t believe on it because it should be right love at the right time. There is no such thing as wrong time because I believe that true love will find a way when the time will come you can feel its presence it would be as magical like in fairy tales and no one can stop you from your happiness. Although for other the wrong time means about the having a relationship at the very young age or having relationship to whom you love but it&rsquo;s against all odds but why rush things on finding love? why defend love when it&rsquo;s against to all odds? We don&rsquo;t need to rush things because every step we will take in our lives is according to Gods plan. We don&rsquo;t need also to defend our love from others because at first, they will not be against if they didn&rsquo;t see anything wrong between you and your love and why will they stop you from being happy when they are confident that you will not be in pain in the future.&nbsp;<br /><br />To whom should I fall in love? that&rsquo;s a question from one of my friends in school. I didn&rsquo;t answer her question that time because I don&rsquo;t really &nbsp;know what&rsquo;s the best answer for that. Although I have an answer in my mind and that is you don&rsquo;t really know to whom should you fall in love because you can&rsquo;t control your feelings. Yes, you might prevent it from falling but still no cure for that feeling. Lets just accept the fact that it&rsquo;s not always whom we want but its all about whom we are destined for.&nbsp;<br /><br />Going back to the cycle of love, as I said I always wonder if there is such a thing like that. Honestly speaking that cycle of love is just a result of being too numb and too crazy about love. Why do I say so? It&rsquo;s because yes, you started as a stranger but it&rsquo;s not appropriate to end it as a strangers also. You have spent days, month and years with that person, you share all your problems with each other and yet you&rsquo;ll just waste your friendship because you separated as lovers? I know it&rsquo;s not that easy to move on but its easy to accept the reality. If you easily accept reality its a way on how to move on. Accept the fact that you will be friends, accept the fact that you&rsquo;ll not be together again. It&rsquo;s just all about accepting all the result of your choice. Love is not all about a &ldquo;win-win&rdquo; game but Love is all about give and take.<br /><br />One of my closest friends asked me if I am afraid to fall in love. I answer her question with a smile and said that I am not afraid to fall in love. Why? because the feeling of being in love and being loved by other is the best feeling I have ever felt.&nbsp;<br /><br />Random thoughts,<br />- KATH</p>', 'a44e1a7abcae1bb3e6e11a622b367b37.jpg', 1, 1, 1),
(21, 'Washday Wednesday', 'Washday-Wednesday', '', '2017-02-05 09:20:35', '0000-00-00 00:00:00', 'asfasfasfasf', '<p style="text-align: center;">Its Wednesday again and It&rsquo;s wash day!&nbsp;Last week I was fan of any shades of blue colored things. I find it relaxing and as a student,&nbsp;<strong>I NEED TO RELEASE SOME STRESS</strong> from the projects, exams and terror teachers I&rsquo;ve encountered. By wearing comfortable jeans, shirt and shoes while reading your favorite book. I can feel some good vibes!</p>\r\n<p style="text-align: center;"><br /><strong>BOOK READING IS A WAY YOU CAN RELEASE STRESS! </strong><br /><br /> I was reading <strong>HOW TO LOVE by KATIE COTUGNO</strong>. The story is all about Reena and Sawyer seeing each other after 3 years since they broke up and patching things up to revive their love story. I can rate this book 4/5. It&rsquo;s because everyone can relate and even me I was totally in a cloud nine when I was reading this. <em><strong>I ALSO PROMOTE READING FOR TEENAGERS LIKE ME!</strong></em><br /><br /><strong>LETS TALK ABOUT CLOTHES!</strong><br /><br /> Wardrobe problems and not enough money to buy some fancy clothes? I do have an advice for you.<br /><br /> If you are a student, do not spend too much money for some branded clothes try to be practical. Always think or ask your self if you really need that thing or not. Its the best way to keep your self from buying things you don&rsquo;t need that much.&nbsp;<br /><br /> If you have a work and you want to be more practical but still fashionable, try to buy clothes when the mall or the shop is at 50%-70% off. Its also a way you can buy some fancy clothes within your budget <br /><br /><strong>BELIEVE ME! YOU MUST SAVE YOUR POCKET FROM BEING EMPTY!</strong><br /><br /> I wasn&rsquo;t able to took some whole body shot for my comfy outfit but the shoes are from<strong> PEOPLE ARE PEOPLE</strong> and I bought it for <strong>Php 299.00</strong> because it was on sale. Shirt and jeans are from <strong>GMALL OF TORIL DEPARTMENT STORE</strong> and it cost <strong>Php 220.00</strong> for the shirt and <strong>Php 320.00</strong> for the Jeans.<em> <strong>The whole comfy outfit costs less than Php. 1000.00 or Php 839.00 to be exact.</strong></em><br /><br /> Its not all about how much it cost but Its all about how you delivery your self within that clothes you&rsquo;re wearing.<br /><br /><strong>BE FASHIONABLE AT THE SAME TIME BE PRACTICAL.</strong><br /><br />- KATH</p>', '17893b45c3b434ef46781ba1246c7109.jpg', 1, 1, 1),
(22, 'Lorem Ipsum', 'Lorem-Ipsum', '', '2017-02-04 14:05:37', '0000-00-00 00:00:00', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean id tempus neque. Integer congue nisi sed arcu tristique dictum. Vivamus ut turpis dui. Donec sodales elit a dignissim tempus. Curabitur felis risus, interdum at nibh a, condimentum consequat mi. Fusce euismod suscipit arcu, at cursus mauris mollis at. Curabitur non sapien convallis, pulvinar arcu vitae, sodales neque. Ut efficitur ligula enim, et pellentesque nisl condimentum non. Quisque pharetra ut purus sed pellentesque. Praesent molestie eleifend ipsum ac pellentesque. Pellentesque augue tellus, maximus porttitor luctus et, blandit ut dui. Donec venenatis scelerisque tortor sed cursus. Vivamus vehicula venenatis velit, ut lobortis neque varius nec. Vivamus viverra dui sed mauris volutpat, nec bibendum lorem lacinia. Quisque cursus nulla eget urna venenatis porttitor. Donec suscipit auctor tellus vel vehicula.</p>\r\n<p>Integer facilisis suscipit nisi id eleifend. In vitae pulvinar ex. Morbi volutpat nisl lacus, ac scelerisque augue vestibulum at. Vivamus faucibus lorem auctor erat rutrum aliquet. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vestibulum tempor vitae massa a euismod. Praesent congue risus vitae risus lobortis tristique. Suspendisse bibendum, nisi sed convallis convallis, ante justo cursus urna, vel placerat dui eros non neque. Duis nulla sem, malesuada in aliquet vel, lacinia malesuada ipsum.</p>\r\n<p>Nam fringilla ipsum ex, non luctus nisi elementum nec. Phasellus dignissim a mi et vehicula. Praesent in velit nec diam maximus vulputate nec quis nulla. Nam tempor dolor urna, ut dignissim lectus varius finibus. In hac habitasse platea dictumst. Fusce non mollis tortor. Suspendisse elementum, orci a fermentum scelerisque, justo quam porta arcu, sit amet sodales erat nibh vel tortor. Aenean non turpis sit amet orci dapibus luctus. Sed consectetur ligula risus. Aenean lobortis libero non faucibus ultricies. Nam ac sapien id enim rhoncus ultricies. Proin neque metus, gravida ac mi et, commodo ultrices velit. Proin varius dui tortor, eu luctus nulla bibendum sagittis. Curabitur eu dolor vitae sem porttitor molestie nec ut arcu.</p>\r\n<p>In hac habitasse platea dictumst. Donec quis euismod magna. Duis in aliquam ex. Etiam mollis pulvinar arcu, sed convallis elit convallis quis. Nulla facilisi. In sed sem et turpis egestas auctor eget eu elit. Suspendisse sollicitudin cursus nibh eget faucibus. Praesent posuere turpis quis velit consequat sodales. Fusce efficitur consectetur sapien, nec efficitur nulla vehicula vitae. Integer a eleifend dolor. Curabitur rhoncus ultricies elementum. Morbi ante lacus, luctus sed elit sit amet, sollicitudin suscipit ante. Sed hendrerit scelerisque ante.</p>', '72436118938731608edb765b5e1ecd88.PNG', 1, 1, 1),
(23, 'Washday Wednesday', 'Washday-Wednesday-1', '', '2017-02-05 07:27:34', '0000-00-00 00:00:00', 'asfasfasfasf', '<p style="text-align: center;">Its Wednesday again and It&rsquo;s wash day!&nbsp;Last week I was fan of any shades of blue colored things. I find it relaxing and as a student,&nbsp;<strong>I NEED TO RELEASE SOME STRESS</strong> from the projects, exams and terror teachers I&rsquo;ve encountered. By wearing comfortable jeans, shirt and shoes while reading your favorite book. I can feel some good vibes!</p>\r\n<p style="text-align: center;"><br /><strong>BOOK READING IS A WAY YOU CAN RELEASE STRESS! </strong><br /><br /> I was reading <strong>HOW TO LOVE by KATIE COTUGNO</strong>. The story is all about Reena and Sawyer seeing each other after 3 years since they broke up and patching things up to revive their love story. I can rate this book 4/5. It&rsquo;s because everyone can relate and even me I was totally in a cloud nine when I was reading this. <em><strong>I ALSO PROMOTE READING FOR TEENAGERS LIKE ME!</strong></em><br /><br /><strong>LETS TALK ABOUT CLOTHES!</strong><br /><br /> Wardrobe problems and not enough money to buy some fancy clothes? I do have an advice for you.<br /><br /> If you are a student, do not spend too much money for some branded clothes try to be practical. Always think or ask your self if you really need that thing or not. Its the best way to keep your self from buying things you don&rsquo;t need that much.&nbsp;<br /><br /> If you have a work and you want to be more practical but still fashionable, try to buy clothes when the mall or the shop is at 50%-70% off. Its also a way you can buy some fancy clothes within your budget <br /><br /><strong>BELIEVE ME! YOU MUST SAVE YOUR POCKET FROM BEING EMPTY!</strong><br /><br /> I wasn&rsquo;t able to took some whole body shot for my comfy outfit but the shoes are from<strong> PEOPLE ARE PEOPLE</strong> and I bought it for <strong>Php 299.00</strong> because it was on sale. Shirt and jeans are from <strong>GMALL OF TORIL DEPARTMENT STORE</strong> and it cost <strong>Php 220.00</strong> for the shirt and <strong>Php 320.00</strong> for the Jeans.<em> <strong>The whole comfy outfit costs less than Php. 1000.00 or Php 839.00 to be exact.</strong></em><br /><br /> Its not all about how much it cost but Its all about how you delivery your self within that clothes you&rsquo;re wearing.<br /><br /><strong>BE FASHIONABLE AT THE SAME TIME BE PRACTICAL.</strong><br /><br />- KATH</p>', '2b67030e2d7b4cb0b7609dabe27d70aa.jpg', 1, 0, 1),
(24, 'Lorem Ipsum', 'Lorem-Ipsum-1', '', '2017-02-09 11:11:28', '0000-00-00 00:00:00', '', '<div id="lipsum">\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris dapibus consectetur risus, ac dapibus dolor fringilla a. Etiam at efficitur velit, ut varius felis. Nunc et lacinia nisi, sed convallis velit. Suspendisse non nibh nunc. Maecenas sit amet dictum ex. Vivamus faucibus odio imperdiet lacus maximus congue. Phasellus fermentum accumsan nunc, eget lobortis leo accumsan ac. Maecenas luctus consequat pretium. Ut lorem eros, viverra non convallis non, ornare porttitor enim. Morbi nec commodo mi. Sed imperdiet hendrerit enim sed dictum. Maecenas velit risus, laoreet ut nisi quis, blandit cursus purus. Mauris facilisis dui sed est placerat, eu scelerisque sem scelerisque. Vivamus sit amet enim pharetra nibh gravida porta non id urna. Integer euismod sagittis dui, id aliquet felis viverra nec.</p>\r\n<p>Vestibulum ut eros sed erat viverra auctor sit amet quis nisi. Ut imperdiet felis sed imperdiet mollis. Aliquam rhoncus vulputate ullamcorper. Ut sapien mauris, malesuada eget enim a, volutpat vehicula urna. Sed vehicula pellentesque vehicula. Nullam risus augue, tempus id enim ornare, porttitor dapibus sem. Pellentesque egestas imperdiet dolor convallis tristique. Morbi ultrices in sapien quis ornare. Suspendisse potenti. Proin suscipit, elit in ornare luctus, ipsum nibh fermentum ex, id luctus purus turpis suscipit sapien. Donec vel ante porttitor, tempus lacus sed, finibus urna. Vivamus dignissim aliquam libero dapibus aliquam.</p>\r\n<p>Aenean ac odio suscipit, consectetur elit eu, posuere ligula. Fusce ligula purus, egestas dignissim erat vel, volutpat commodo metus. Nullam venenatis gravida tincidunt. Morbi malesuada arcu nunc, ac pretium sapien porttitor id. Vivamus a nulla ac magna iaculis tempor. Ut nec ipsum felis. Nam volutpat, nisi quis efficitur rutrum, leo sapien tempus massa, convallis gravida velit sem a erat. Nulla tincidunt metus sit amet consequat condimentum.</p>\r\n<p>Aenean dolor neque, congue vitae arcu sit amet, porta tincidunt ante. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vestibulum lorem interdum sodales pretium. Aliquam posuere fermentum purus non hendrerit. Ut molestie molestie ipsum nec vulputate. Duis vulputate nec dui ac tempus. Proin lectus libero, ullamcorper ac tortor id, vehicula pellentesque ante. Vestibulum finibus interdum lorem, vitae tristique quam pharetra et. Nunc lorem ex, vulputate sed semper vel, condimentum id enim. Cras suscipit consequat ante ut vestibulum. Vestibulum ullamcorper ultricies nulla, lobortis sagittis lectus pulvinar non. Vestibulum volutpat sagittis pretium. Nam id sagittis felis.</p>\r\n<p>Suspendisse ac faucibus velit. Duis lobortis ornare nibh vitae venenatis. Maecenas in turpis congue, ultricies lacus et, bibendum enim. Nam malesuada nibh ut tempus ultricies. Suspendisse mollis, ipsum eu condimentum interdum, nisi odio consectetur sapien, a congue ligula lectus quis leo. Donec id dolor ligula. Phasellus dui nisi, viverra et cursus et, facilisis sed felis.</p>\r\n</div>', '374a843bf41fa12f887b75f43c64fc58.PNG', 1, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `ha_days`
--

CREATE TABLE `ha_days` (
  `day_id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL,
  `day_title` varchar(250) NOT NULL,
  `day_content` text NOT NULL,
  `day_photos` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ha_days`
--

INSERT INTO `ha_days` (`day_id`, `blog_id`, `day_title`, `day_content`, `day_photos`) VALUES
(1, 6, 'Day Title', '<p>day content</p>', ',,,,'),
(2, 7, 'Day Title', '<p>day content</p>', '945b078eb9405f264b6434f3e6c2139c.PNG,7351a730d4b787b9f72e05375ba7d998.PNG,e1e74e763cadac08c25497f8a860706d.PNG,f8e34daa70ac078a5ee2f7576c1ef0d3.PNG,c4b4137d6ace837676cca11c6c8c3904.PNG'),
(3, 8, 'Day Title', '<p>day content</p>', '2e36f7fc12d0d05ccc0487dfe27ac022.PNG,3eefd1c3a7fe43062cb6840a8c5750c6.PNG,a9f261059473d16d6c8ae2a69f5ef58c.PNG,32a19066def18403d6c5e1eb65755844.PNG,98ad2c163761319acc9e0e58900476f7.PNG'),
(4, 9, 'test day 1', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at tincidunt risus. Mauris ac varius ex, id pulvinar ligula. Donec ipsum nisl, consectetur nec lorem ut, luctus facilisis dolor. Vivamus dignissim iaculis dui ac iaculis. Vestibulum fermentum lectus sit amet diam scelerisque varius. Aenean lorem sem, porta nec molestie efficitur, faucibus quis arcu. Pellentesque eu placerat enim. Pellentesque ut lorem orci. Pellentesque feugiat fermentum libero, et fringilla enim egestas a. Phasellus et fermentum felis, eget interdum tellus. Curabitur ac consequat lacus. Vivamus ac consequat elit. Pellentesque rhoncus sapien at ultricies ornare. Ut et mi tincidunt, feugiat ex et, ultrices velit. Nunc lobortis fringilla rutrum.</p>\r\n<p>Quisque semper metus massa. Integer nibh augue, hendrerit nec laoreet et, lobortis et ex. Curabitur magna arcu, accumsan sit amet ligula in, tempus consequat lacus. Quisque eget dolor eget ante fringilla rutrum ut nec nulla. Integer vitae augue ut mauris sagittis fringilla. Nam interdum mi eget nibh auctor tempor. Proin lobortis vitae sapien vel sagittis. Aliquam rutrum sapien varius pulvinar sodales. Suspendisse ac nulla eu ligula pulvinar congue. Morbi condimentum turpis ante, eget elementum enim laoreet vitae. Sed tincidunt eget arcu eget convallis. Sed nunc justo, rhoncus a urna eleifend, efficitur dapibus sapien. Quisque ultrices sodales dolor eget bibendum. Phasellus a dignissim sapien. Nullam auctor libero laoreet magna rutrum, ac sollicitudin metus venenatis. Integer eu nibh maximus, tristique ipsum ut, consequat sapien.</p>\r\n<p>Cras semper dolor mi, a iaculis nisl ullamcorper et. In vel justo luctus, vehicula sem quis, imperdiet dolor. Nunc ullamcorper magna vel nibh dignissim auctor. Duis vitae semper est, non porttitor dui. Suspendisse cursus, sem vel semper condimentum, ipsum risus elementum nunc, non dignissim quam orci vitae nibh. Phasellus et quam suscipit, consequat ipsum sed, hendrerit lorem. Nam aliquam dolor quis velit pulvinar egestas. Nulla sed turpis posuere, facilisis turpis facilisis, pharetra est. Pellentesque pulvinar velit est, nec viverra massa commodo tincidunt. Maecenas egestas lacinia sodales.</p>', ',1c37d0c4700c63e45ed61e89257bb341.JPG,,,12480782b0dbe7a96e1f609090296b7d.JPG'),
(5, 21, 'test 123123123123123123', '<p style="text-align: right;">Its Wednesday again and It&rsquo;s wash day!&nbsp;Last week I was fan of any shades of blue colored things. I find it relaxing and as a student,&nbsp;<strong>I NEED TO RELEASE SOME STRESS</strong> from the projects, exams and terror teachers I&rsquo;ve encountered. By wearing comfortable jeans, shirt and shoes while reading your favorite book. I can feel some good vibes!</p>\r\n<p style="text-align: right;"><br /><strong>BOOK READING IS A WAY YOU CAN RELEASE STRESS! </strong><br /><br /> I was reading <strong>HOW TO LOVE by KATIE COTUGNO</strong>. The story is all about Reena and Sawyer seeing each other after 3 years since they broke up and patching things up to revive their love story. I can rate this book 4/5. It&rsquo;s because everyone can relate and even me I was totally in a cloud nine when I was reading this. <em><strong>I ALSO PROMOTE READING FOR TEENAGERS LIKE ME!</strong></em><br /><br /><strong>LETS TALK ABOUT CLOTHES!</strong><br /><br /> Wardrobe problems and not enough money to buy some fancy clothes? I do have an advice for you.<br /><br /> If you are a student, do not spend too much money for some branded clothes try to be practical. Always think or ask your self if you really need that thing or not. Its the best way to keep your self from buying things you don&rsquo;t need that much.&nbsp;<br /><br /> If you have a work and you want to be more practical but still fashionable, try to buy clothes when the mall or the shop is at 50%-70% off. Its also a way you can buy some fancy clothes within your budget <br /><br /><strong>BELIEVE ME! YOU MUST SAVE YOUR POCKET FROM BEING EMPTY!</strong><br /><br /> I wasn&rsquo;t able to took some whole body shot for my comfy outfit but the shoes are from<strong> PEOPLE ARE PEOPLE</strong> and I bought it for <strong>Php 299.00</strong> because it was on sale. Shirt and jeans are from <strong>GMALL OF TORIL DEPARTMENT STORE</strong> and it cost <strong>Php 220.00</strong> for the shirt and <strong>Php 320.00</strong> for the Jeans.<em> <strong>The whole comfy outfit costs less than Php. 1000.00 or Php 839.00 to be exact.</strong></em><br /><br /> Its not all about how much it cost but Its all about how you delivery your self within that clothes you&rsquo;re wearing.<br /><br /><strong>BE FASHIONABLE AT THE SAME TIME BE PRACTICAL.</strong><br /><br />- KATH</p>', '1729c72525db78ca4da89f27647eb81e.jpg'),
(6, 21, 'test2135 135 135 135 135 135 13 5135 ', '<p style="text-align: justify;">Its Wednesday a135 135 135 135 gain and It&rsquo;s wash day!&nbsp;Last week I was fan of any shades of blue colored things. I find it relaxing and as a student,&nbsp;<strong>I NEED TO RELEASE SOME STRESS</strong> from the projects, exams and terror teachers I&rsquo;ve encountered. By wearing comfortable jeans, shirt and shoes while reading your favorite book. I can feel some good vibes!</p>\r\n<p style="text-align: justify;"><br /><strong>BOOK READING IS A WAY YOU CAN RELEASE STRESS! </strong><br /><br /> I was reading <strong>HOW TO LOVE by KATIE COTUGNO</strong>. The story is all about Reena and Sawyer seeing each other after 3 years since they broke up and patching things up to revive their love story. I can rate this book 4/5. It&rsquo;s because everyone can relate and even me I was totally in a cloud nine when I was reading this. <em><strong>I ALSO PROMOTE READING FOR TEENAGERS LIKE ME!</strong></em><br /><br /><strong>LETS TALK ABOUT CLOTHES!</strong><br /><br /> Wardrobe problems and not enough money to buy some fancy clothes? I do have an advice for you.<br /><br /> If you are a student, do not spend too much money for some branded clothes try to be practical. Always think or ask your self if you really need that thing or not. Its the best way to keep your self from buying things you don&rsquo;t need that much.&nbsp;<br /><br /> If you have a work and you want to be more practical but still fashionable, try to buy clothes when the mall or the shop is at 50%-70% off. Its also a way you can buy some fancy clothes within your budget <br /><br /><strong>BELIEVE ME! YOU MUST SAVE YOUR POCKET FROM BEING EMPTY!</strong><br /><br /> I wasn&rsquo;t able to took some whole body shot for my comfy outfit but the shoes are from<strong> PEOPLE ARE PEOPLE</strong> and I bought it for <strong>Php 299.00</strong> because it was on sale. Shirt and jeans are from <strong>GMALL OF TORIL DEPARTMENT STORE</strong> and it cost <strong>Php 220.00</strong> for the shirt and <strong>Php 320.00</strong> for the Jeans.<em> <strong>The whole comfy outfit costs less than Php. 1000.00 or Php 839.00 to be exact.</strong></em><br /><br /> Its not all about how much it cost but Its all about how you delivery your self within that clothes you&rsquo;re wearing.<br /><br /><strong>BE FASHIONABLE AT THE SAME TIME BE PRACTICAL.</strong><br /><br />- KATH</p>', '6358e4edcfdc6c213f29b653cad8b52f.PNG'),
(7, 23, 'test 123123123123123123', '<p style="text-align: right;">Its Wednesday again and It&rsquo;s wash day!&nbsp;Last week I was fan of any shades of blue colored things. I find it relaxing and as a student,&nbsp;<strong>I NEED TO RELEASE SOME STRESS</strong> from the projects, exams and terror teachers I&rsquo;ve encountered. By wearing comfortable jeans, shirt and shoes while reading your favorite book. I can feel some good vibes!</p>\r\n<p style="text-align: right;"><br /><strong>BOOK READING IS A WAY YOU CAN RELEASE STRESS! </strong><br /><br /> I was reading <strong>HOW TO LOVE by KATIE COTUGNO</strong>. The story is all about Reena and Sawyer seeing each other after 3 years since they broke up and patching things up to revive their love story. I can rate this book 4/5. It&rsquo;s because everyone can relate and even me I was totally in a cloud nine when I was reading this. <em><strong>I ALSO PROMOTE READING FOR TEENAGERS LIKE ME!</strong></em><br /><br /><strong>LETS TALK ABOUT CLOTHES!</strong><br /><br /> Wardrobe problems and not enough money to buy some fancy clothes? I do have an advice for you.<br /><br /> If you are a student, do not spend too much money for some branded clothes try to be practical. Always think or ask your self if you really need that thing or not. Its the best way to keep your self from buying things you don&rsquo;t need that much.&nbsp;<br /><br /> If you have a work and you want to be more practical but still fashionable, try to buy clothes when the mall or the shop is at 50%-70% off. Its also a way you can buy some fancy clothes within your budget <br /><br /><strong>BELIEVE ME! YOU MUST SAVE YOUR POCKET FROM BEING EMPTY!</strong><br /><br /> I wasn&rsquo;t able to took some whole body shot for my comfy outfit but the shoes are from<strong> PEOPLE ARE PEOPLE</strong> and I bought it for <strong>Php 299.00</strong> because it was on sale. Shirt and jeans are from <strong>GMALL OF TORIL DEPARTMENT STORE</strong> and it cost <strong>Php 220.00</strong> for the shirt and <strong>Php 320.00</strong> for the Jeans.<em> <strong>The whole comfy outfit costs less than Php. 1000.00 or Php 839.00 to be exact.</strong></em><br /><br /> Its not all about how much it cost but Its all about how you delivery your self within that clothes you&rsquo;re wearing.<br /><br /><strong>BE FASHIONABLE AT THE SAME TIME BE PRACTICAL.</strong><br /><br />- KATH</p>', 'ee31248ebf29a7baa3cc056181000b57.jpg'),
(8, 23, 'test2135 135 135 135 135 135 13 5135 ', '<p style="text-align: justify;">Its Wednesday a135 135 135 135 gain and It&rsquo;s wash day!&nbsp;Last week I was fan of any shades of blue colored things. I find it relaxing and as a student,&nbsp;<strong>I NEED TO RELEASE SOME STRESS</strong> from the projects, exams and terror teachers I&rsquo;ve encountered. By wearing comfortable jeans, shirt and shoes while reading your favorite book. I can feel some good vibes!</p>\r\n<p style="text-align: justify;"><br /><strong>BOOK READING IS A WAY YOU CAN RELEASE STRESS! </strong><br /><br /> I was reading <strong>HOW TO LOVE by KATIE COTUGNO</strong>. The story is all about Reena and Sawyer seeing each other after 3 years since they broke up and patching things up to revive their love story. I can rate this book 4/5. It&rsquo;s because everyone can relate and even me I was totally in a cloud nine when I was reading this. <em><strong>I ALSO PROMOTE READING FOR TEENAGERS LIKE ME!</strong></em><br /><br /><strong>LETS TALK ABOUT CLOTHES!</strong><br /><br /> Wardrobe problems and not enough money to buy some fancy clothes? I do have an advice for you.<br /><br /> If you are a student, do not spend too much money for some branded clothes try to be practical. Always think or ask your self if you really need that thing or not. Its the best way to keep your self from buying things you don&rsquo;t need that much.&nbsp;<br /><br /> If you have a work and you want to be more practical but still fashionable, try to buy clothes when the mall or the shop is at 50%-70% off. Its also a way you can buy some fancy clothes within your budget <br /><br /><strong>BELIEVE ME! YOU MUST SAVE YOUR POCKET FROM BEING EMPTY!</strong><br /><br /> I wasn&rsquo;t able to took some whole body shot for my comfy outfit but the shoes are from<strong> PEOPLE ARE PEOPLE</strong> and I bought it for <strong>Php 299.00</strong> because it was on sale. Shirt and jeans are from <strong>GMALL OF TORIL DEPARTMENT STORE</strong> and it cost <strong>Php 220.00</strong> for the shirt and <strong>Php 320.00</strong> for the Jeans.<em> <strong>The whole comfy outfit costs less than Php. 1000.00 or Php 839.00 to be exact.</strong></em><br /><br /> Its not all about how much it cost but Its all about how you delivery your self within that clothes you&rsquo;re wearing.<br /><br /><strong>BE FASHIONABLE AT THE SAME TIME BE PRACTICAL.</strong><br /><br />- KATH</p>', '474aa9f837d23ae013a7ea553bf6170c.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `ha_expenses`
--

CREATE TABLE `ha_expenses` (
  `expense_id` int(11) NOT NULL,
  `day_id` int(11) NOT NULL,
  `expense_description` varchar(120) NOT NULL,
  `expense_amount` decimal(11,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ha_expenses`
--

INSERT INTO `ha_expenses` (`expense_id`, `day_id`, `expense_description`, `expense_amount`) VALUES
(1, 3, '123', '231.00'),
(2, 4, 'Fare', '1200.00'),
(3, 5, '123135', '21135.00'),
(4, 5, 'Fare135', '195315.00'),
(5, 6, '134135', '134135.00'),
(6, 6, '134135', '314135.00'),
(7, 7, '123135', '21135.00'),
(8, 7, 'Fare135', '195315.00'),
(9, 8, '134135', '134135.00'),
(10, 8, '134135', '314135.00'),
(11, 8, 'asfasfasfasf', '2123.00'),
(12, 6, 'asfasfasf', '123.00'),
(13, 6, 'asfasfasf', '123.00');

-- --------------------------------------------------------

--
-- Table structure for table `ha_itinerary`
--

CREATE TABLE `ha_itinerary` (
  `itinerary_id` int(11) NOT NULL,
  `day_id` int(11) NOT NULL,
  `itinerary_time` varchar(50) NOT NULL,
  `itinerary_description` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ha_itinerary`
--

INSERT INTO `ha_itinerary` (`itinerary_id`, `day_id`, `itinerary_time`, `itinerary_description`) VALUES
(1, 3, '123', '123'),
(2, 4, '7:30 AM', 'Testing'),
(3, 5, '10:00 AM', 'Testing135'),
(4, 5, '9:00 PM', 'UWI135'),
(5, 5, '10:00 AM', '123135'),
(6, 6, '10:00 Am', 'asd135'),
(7, 6, '10:00 Am', 'aaas135'),
(8, 6, '10:00 Am', 'sdsad135'),
(9, 7, '10:00 AM', 'Testing135'),
(10, 7, '9:00 PM', 'UWI135'),
(11, 7, '10:00 AM', '123135'),
(12, 8, '10:00 Am', 'asd135'),
(13, 8, '10:00 Am', 'aaas135'),
(14, 8, '10:00 Am', 'sdsad135'),
(15, 6, '10:00 Am', 'asd135asfasf'),
(16, 6, '10:00 Am', 'asd135asfasf'),
(17, 6, '10:00 Am', 'asd135asfasf');

-- --------------------------------------------------------

--
-- Table structure for table `ha_menus`
--

CREATE TABLE `ha_menus` (
  `menu_id` int(11) NOT NULL,
  `menu_href` varchar(250) NOT NULL COMMENT 'segment only.',
  `menu_name` varchar(50) NOT NULL,
  `menu_status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ha_menus`
--

INSERT INTO `ha_menus` (`menu_id`, `menu_href`, `menu_name`, `menu_status`) VALUES
(2, 'page/Lorem-Ipsum-1', 'Lorem Ipsum', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ha_settings`
--

CREATE TABLE `ha_settings` (
  `setting_id` int(11) NOT NULL,
  `setting_code` varchar(65) NOT NULL,
  `setting_value` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ha_settings`
--

INSERT INTO `ha_settings` (`setting_id`, `setting_code`, `setting_value`) VALUES
(1, 'title', '#whereyouatkath'),
(2, 'sub_title', 'K A T H H I N L O G'),
(3, 'ex_tagline', 'The world is a book, and those who do not travel read only a page.'),
(4, 'maintainance', '0'),
(5, 'copyright', 'Kath Hinlog');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ha_authors`
--
ALTER TABLE `ha_authors`
  ADD PRIMARY KEY (`author_id`);

--
-- Indexes for table `ha_banners`
--
ALTER TABLE `ha_banners`
  ADD PRIMARY KEY (`banner_id`);

--
-- Indexes for table `ha_blog`
--
ALTER TABLE `ha_blog`
  ADD PRIMARY KEY (`blog_id`),
  ADD UNIQUE KEY `blog_slug` (`blog_slug`);

--
-- Indexes for table `ha_days`
--
ALTER TABLE `ha_days`
  ADD PRIMARY KEY (`day_id`);

--
-- Indexes for table `ha_expenses`
--
ALTER TABLE `ha_expenses`
  ADD PRIMARY KEY (`expense_id`);

--
-- Indexes for table `ha_itinerary`
--
ALTER TABLE `ha_itinerary`
  ADD PRIMARY KEY (`itinerary_id`);

--
-- Indexes for table `ha_menus`
--
ALTER TABLE `ha_menus`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `ha_settings`
--
ALTER TABLE `ha_settings`
  ADD PRIMARY KEY (`setting_id`),
  ADD UNIQUE KEY `setting_code` (`setting_code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ha_authors`
--
ALTER TABLE `ha_authors`
  MODIFY `author_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ha_banners`
--
ALTER TABLE `ha_banners`
  MODIFY `banner_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ha_blog`
--
ALTER TABLE `ha_blog`
  MODIFY `blog_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `ha_days`
--
ALTER TABLE `ha_days`
  MODIFY `day_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `ha_expenses`
--
ALTER TABLE `ha_expenses`
  MODIFY `expense_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `ha_itinerary`
--
ALTER TABLE `ha_itinerary`
  MODIFY `itinerary_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `ha_menus`
--
ALTER TABLE `ha_menus`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ha_settings`
--
ALTER TABLE `ha_settings`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
